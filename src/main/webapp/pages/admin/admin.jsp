<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>
<body>
<a id="gh" href="periodical?command=home.Home" class="btn btn-outline-primary mt-1">
<fmt:message bundle="${rb}" key="icon.back"/></a>
<h2 id="error_admin" class="text-danger"></h2>
<%@include file="modal_admin.jspf" %>
<div class="container">
	<div id="admin" class="row">
        <div class="span12">
    		<table class="table table-condensed table-hover">
    			<thead>
    				<tr>
    					<th>№</th>
    					<th><fmt:message bundle="${rb}" key="page.admin.role"/></th>
    					<th><fmt:message bundle="${rb}" key="page.admin.nick"/></th>
    					<th><fmt:message bundle="${rb}" key="page.admin.email"/></th>
    					<th><fmt:message bundle="${rb}" key="page.admin.balance"/></th>
    					<th><fmt:message bundle="${rb}" key="page.admin.subscriptions"/></th>
    					<th><fmt:message bundle="${rb}" key="page.admin.sum"/></th>
    					<th><fmt:message bundle="${rb}" key="page.admin.blocked"/></th>
    				</tr>
    			</thead>
    			<tbody>
    			<c:forEach var="elem" items="${users}"
                			varStatus="status">
                	<input type="hidden" name="id_user${status.count}" value="${elem.user.id}">
                	<tr>
                       <td>${status.count}</td>
                       <td>
                       <select id="role${status.count}" class="form-select" name="${elem.user.id}">
                       <option value="ADMIN" <c:if test="${elem.user.role == 'ADMIN'}">selected</c:if>>ADMIN</option>
                       <option value="USER" <c:if test="${elem.user.role == 'USER'}">selected</c:if>>USER</option>
                       </select></td>
                       <td><a href="periodical?command=guest.Registration&type_operation=edit&admin_id=${elem.user.id}"
                       class="btn btn-info">${elem.user.nickName}</a></td>
                       <td>${elem.user.email}</td>
                       <td>${elem.user.balance}</td>
                       <td><a href="periodical?command=admin.ShowSalesUser&admin_id=${elem.user.id}" class="btn btn-primary">${elem.count}</a></td>
                       <td>${elem.sum}</td>
                       <td>
                       <div class="form-check form-switch">
                       <input id="${elem.user.id}" class="form-check-input" type="checkbox" name="blocked" value="true"
                       <c:if test="${elem.user.blocked}">checked</c:if>/>
                       <label class="form-check-label" for="mySwitch">Blocked</label>
                       </div>
                       </td>
                    </tr>
                </c:forEach>
    			</tbody>
    		</table>
    	</div>
	</div>
</div>
</body>
</html>