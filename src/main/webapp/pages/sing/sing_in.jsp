<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
</head>
<body>

<div class="card position-absolute top-50 start-50 translate-middle">
<article class="card-body">
    <div class="form-group">
    <a href="periodical?command=home.Home" class="btn btn-outline-primary mt-1">
    <fmt:message bundle="${rb}" key="icon.back"/></a>
	<h4 class="card-title text-center mb-4 mt-1"><fmt:message bundle="${rb}" key="sing.in"/></h4>
	</div>
	<hr>
	<p class="text-success text-center"><fmt:message bundle="${rb}" key="sing.enter"/></p>
	<form action="periodical?command=guest.EnterUser" method="POST">
	<div class="form-group">
	<div class="input-group">
		<div class="input-group-prepend">
		    <span class="input-group-text"> <i class="fa fa-user"></i> </span>
		 </div>
		<input name="login" class="form-control" placeholder="Enter login" type="text">
	</div> <!-- input-group.// -->
	</div> <!-- form-group// -->
	<div class="form-group">
	<div class="input-group">
		<div class="input-group-prepend">
		    <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
		 </div>
	    <input name="password" class="form-control" placeholder="******" type="password">
	</div> <!-- input-group.// -->
	<small class="form-text text-danger">${incorrect_login}</small>
	</div> <!-- form-group// -->
	<div class="form-group">
	<button type="submit" class="btn btn-primary btn-block">
	<fmt:message bundle="${rb}" key="registration.login"/>  </button>
	</div> <!-- form-group// -->
	</form>
</article>
</div>

</body>
</html>