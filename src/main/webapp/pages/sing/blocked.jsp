<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
</head>
<body>
<h2>><fmt:message bundle="${rb}" key="modal.sale.date"/> <br> ${text_blocked}</h2>
<a href="periodical?command=home.Home"><fmt:message bundle="${rb}" key="modal.sale.date"/></a>
</body>
</html>