<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div>
	<div id="modalSubCatalogs">
		<c:forEach var="elem" items="${list_catalogs}" varStatus="n_catalog">
			<div class="row mb-3">
				<select class="form-select"
					<c:if test="${!n_catalog.last}">onchange="changeCatalog('change')"</c:if>
					name="catalog${n_catalog.count}">
					<c:forEach var="el" items="${elem}" varStatus="n_sub_catalog">
						<option value="${n_sub_catalog.count}"
							<c:if test="${el.active}">selected</c:if>>${el.name}</option>
					</c:forEach>
				</select>
			</div>
		</c:forEach>
	</div>
	<div class="row mb-3">
		<button id="addCatalog" type="button" class="btn btn-primary col-1"
			onclick="changeCatalog('add')">+</button>
		<button id="removeCatalog" type="button" class="btn btn-primary col-1"
			onclick="remCatalog()">-</button>
		<label for="addCatalog" class="col-sm-5 col-form-label col-form-label-sm">
		<fmt:message bundle="${rb}" key="modal.catalog.attach"/></label>
	</div>
	<c:if test="${typeOperation == 'edit'}">
		<div id="oldName" class="row mb-3">
			<label for="oldCatalog" class="col-sm-5 col-form-label">
			<fmt:message bundle="${rb}" key="modal.catalog.oldName"/></label>
			<label id="oldCatalog" class="col-sm-5 col-form-label">${oldName}</label>
			<input type="hidden" name="oldIdCatalog" value="${oldId}">
		</div>
	</c:if>
</div>

