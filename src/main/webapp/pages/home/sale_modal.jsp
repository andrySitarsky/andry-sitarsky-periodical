<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="input-group">
			<span class="input-group-text"><fmt:message bundle="${rb}" key="modal.sale.date"/></span>
			<input type="date"class="form-control" name="now_data_sale"
			  value="${now_data_sale}">
            <span class="input-group-text"><fmt:message bundle="${rb}" key="user.namePeriodical"/>: ${name_periodical}</span>
            <input type="hidden" class="form-control" value="${id_periodical_sale}" name="id_periodical_sale"/>

            <span class="input-group-text"><fmt:message bundle="${rb}" key="modal.sale.price"/> ${price_sale}</span>
            <input id="price_sale" type="hidden" class="form-control" value="${price_sale}" name="price_sale"/>

            </div>