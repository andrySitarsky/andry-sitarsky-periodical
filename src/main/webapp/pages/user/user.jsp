<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
</head>
<body>
<a href="<c:choose><c:when test='${admin}'>periodical?command=admin.AdminPage</c:when>
         <c:otherwise>periodical?command=home.Home</c:otherwise></c:choose>" class="btn btn-outline-primary mt-1">Back</a>
<div class="container">
	<div class="row">
        <div class="span12">
    		<table class="table table-condensed table-hover">
    			<thead>
    				<tr>
    					<th><fmt:message bundle="${rb}" key="user.dateSubscribe"/></th>
    					<th><fmt:message bundle="${rb}" key="user.namePeriodical"/></th>
    					<th><fmt:message bundle="${rb}" key="user.startDate"/></th>
    					<th><fmt:message bundle="${rb}" key="user.endDate"/></th>
    					<th><fmt:message bundle="${rb}" key="user.number"/></th>
    					<th><fmt:message bundle="${rb}" key="page.home.card.pSubscription"/></th>
    				</tr>
    			</thead>
    			<tbody>
    			<c:forEach var="elem" items="${sales}"
                			varStatus="status">
                	<tr>
                       <td>${elem.date}</td>
                       <td>${elem.namePeriodical}</td>
                       <td>${elem.dateStart}</td>
                       <td>${elem.dateEnd}</td>
                       <td>${elem.number}</td>
                       <td>${elem.amount}</td>
                    </tr>
                </c:forEach>
    			</tbody>
    		</table>
    	</div>
	</div>
</div>
</body>
</html>