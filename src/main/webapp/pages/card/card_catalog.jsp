<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="/WEB-INF/tld/menuTag.tld" prefix="mt"%>
<div>
	<c:forEach var="element" items="${card_catalogs}"
		varStatus="full_catalog">
		<div class="row">
		<c:if test="${type_operation == 'edit' || type_operation == 'add'}">
			<span class="btn btn-primary col-1" data-bs-toggle="modal"
				data-bs-target="#myModal"
				onclick="getCatalogs('edit','${element.level}')">+</span>
        	</c:if>
			<p class="card-text col-4"><mt:itemMenu list="${catalog_serves.catalogs}"
			topic="${element.level}" bundle="${rb}"/></p>
		</div>
	</c:forEach>
	<c:if test="${type_operation == 'edit' || type_operation == 'add'}">
	<input type="hidden" name="count_catalogs" value="${count_catalogs}">
	<button type="button" class="btn btn-primary"
    		onclick="getCatalogsCard('-1')"><fmt:message bundle="${rb}" key="button.add"/></button>
	<button type="button" class="btn btn-primary"
		onclick="getCatalogsCard('0')"><fmt:message bundle="${rb}" key="button.delete"/></button>
	</c:if>
</div>