<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class ="row" id="idPrice">
	<c:forEach var="el" items="${card_data}" varStatus="status">
		<c:choose>

			<c:when test="${type_operation == 'edit' || type_operation == 'add'}">
				<div class="input-group">
					<span class="input-group-text"><fmt:message bundle="${rb}" key="modal.sale.from"/>
					</span> <input type="date"
						class="form-control" name="from_data${status.count}"
						<c:if test="${type_operation == 'edit'}">
                                        value="${el.startData}" </c:if>/>
					<span class="input-group-text"><fmt:message bundle="${rb}" key="modal.sale.to"/>
					</span> <input type="date"
						class="form-control" name="to_data${status.count}"
						<c:if test="${type_operation == 'edit'}">
                                        value="${el.endData}" </c:if>/>
					<span class="input-group-text"><fmt:message bundle="${rb}" key="modal.sale.price"/>
					</span> <input type="number" step='0.01'  min='0'
						class="form-control" name="new_price${status.count}"
						<c:if test="${type_operation == 'edit'}">
                                        placeholder="${el.price}" </c:if>/>
					<span class="input-group-text"><fmt:message bundle="${rb}" key="icon.money"/></span>
				</div>
			</c:when>
			<c:otherwise>
			<div>
				<label class="form-label col">з ${el.startData} по
					${el.endData} ціна ${el.price} грн</label>
					</div>
			</c:otherwise>
		</c:choose>

	</c:forEach>
</div>
<c:if test="${type_operation == 'edit' || type_operation == 'add'}">
<small class="form-text text-danger">${errorPrice}</small>
<small class="form-text text-danger">${errorPriceNew}</small>
</c:if>