<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>
<body>
	<%@include file="../home/jspf/head.jspf"%>
	<a href="periodical?command=Home" class="btn btn-outline-primary mt-1">Back</a>
	<form action="periodical?command=admin.WriteCard" method="POST" enctype="multipart/form-data">
	<div class="card">
		<div class="row">
			<div class="col-4">
				<img class="img-fluid rounded-start" src="${card.pathImage}"
					alt="Card image">
					<c:if test="${type_operation == 'edit' || type_operation == 'add'}">
				<input name="file" type="file"/>
                    	</c:if>
			</div>
			<div class="col-8">
				<div class="card-body">
					<%@include file="jspf/name_card.jspf"%>

					<p class="card-text">Сумм підписки ${card.price} грн</p>
					<%@include file="jspf/catalogs_card.jspf"%>
					<%@include file="jspf/price_card.jspf"%>
					<%@include file="jspf/info_card.jspf"%>
				</div>
				<div class="card-footer row">
				<c:if test="${type_operation == 'edit' || type_operation == 'add'}">
				<input type="submit" class="btn btn-primary col-3" id="submitCard" value="Send">
                </c:if>
                <c:if test="${role == 'USER'}">
					<button type="button" class="btn btn-primary col-3" id="addCatalog" data-bs-toggle="modal"
					data-bs-target="#sale" onclick="openSale('${card_id}')">Підписатись</button>
					</c:if>
					<c:if test="${!edit_page && role == 'ADMIN'}">
					<button type="button" class="btn btn-primary col-3"
						id="removeCatalog">Редагувати</button>
						</c:if>
				</div>
			</div>
		</div>
	</div>
	</form>
</body>
</html>