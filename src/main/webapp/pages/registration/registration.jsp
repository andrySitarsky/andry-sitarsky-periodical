<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
</head>
<body>
<div class="row justify-content-center">
<div class="col-md-6">
<div class="card">
<header class="card-header">
<a href="<c:choose><c:when test='${admin}'>periodical?command=AdminPage</c:when>
<c:otherwise>periodical?command=Home</c:otherwise></c:choose>" class="btn btn-outline-primary mt-1">
<fmt:message bundle="${rb}" key="icon.back"/></a>
	<h4 class="card-title mt-2"><fmt:message bundle="${rb}" key="page.home.card.SignUp"/></h4>
</header>
<article class="card-body">
<form action="periodical?command=guest.WriteRegistration" method="POST">
	<div class="row">
	    <c:if test="${admin}"><input type="hidden" name="admin_id" value="${admin_id}"></c:if>
		<div class="col form-group">
			<label><fmt:message bundle="${rb}" key="registration.firstName"/></label>
		  	<input type="text" name="first_name" class="form-control"
		  	value="${userInfo.firstName}" placeholder="Enter name">
		</div> <!-- form-group end.// -->
		<div class="col form-group">
			<label><fmt:message bundle="${rb}" key="registration.lastName"/></label>
		  	<input type="text" name="last_name" class="form-control"
            value="${userInfo.lastName}" placeholder="Enter name">
		</div> <!-- form-group end.// -->
	</div> <!-- form-row end.// -->
	<div class="row">
    		<div class="col form-group">
    			<label><fmt:message bundle="${rb}" key="registration.nickName"/></label>
    		  	<input type="text" name="nick_name" class="form-control"
                value="${user.nickName}" placeholder="Enter name">
    		</div> <!-- form-group end.// -->
    		<div class="col form-group">
    			<label><fmt:message bundle="${rb}" key="registration.city"/></label>
    		  	<input type="text" name="city" class="form-control"
                value="${userInfo.city}" placeholder="Enter city">
    		</div> <!-- form-group end.// -->
    	</div> <!-- form-row end.// -->
    <div class="form-group">
        			<label><fmt:message bundle="${rb}" key="registration.birth"/></label>
        		  	<input type="date" name="date_birth" class="form-control"
                    value="${userInfo.date}">
                    <small class="form-text text-danger">${error_date_birth}</small>
        		</div> <!-- form-group end.// -->
	<div class="form-group">
		<label><fmt:message bundle="${rb}" key="registration.email"/></label>
		<input type="email" name="email" class="form-control"
        value="${user.email}" placeholder="Enter email">
        <small class="form-text text-danger">${error_email}</small>
	</div> <!-- form-group end.// -->
	<div class="form-group">
			<label class="form-check form-check-inline">
		  <input class="form-check-input" type="radio" name="gender" value="man"
		  <c:if test="${userInfo.gender.name() == 'MAN'}"> checked</c:if>>
		  <span class="form-check-label"> <fmt:message bundle="${rb}" key="registration.man"/> </span>
		</label>
		<label class="form-check form-check-inline">
		  <input class="form-check-input" type="radio" name="gender" value="woman"
		  <c:if test="${userInfo.gender.name() == 'WOMAN'}"> checked</c:if>>
		  <span class="form-check-label"> <fmt:message bundle="${rb}" key="registration.woman"/> </span>
		</label>
	</div> <!-- form-group end.// -->

	<div class="row">
	<div class="col-5 form-group">
    			<label><fmt:message bundle="${rb}" key="registration.login"/> </label>
    		  	<input type="text" name="login" class="form-control" value="${user.login}"
    		  	placeholder="Enter login">
    		  	<small class="form-text text-danger">${error_login}</small>
    		</div> <!-- form-group end.// -->
	<div class="col-7 form-group">
		<label><fmt:message bundle="${rb}" key="registration.password"/></label>
	    <input class="form-control" name="password" type="password">
	</div> <!-- form-group end.// -->
	</div> <!-- form-row end.// -->
    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block">
        <fmt:message bundle="${rb}" key="registration.register"/>  </button>
    </div> <!-- form-group// -->
</form>
</article> <!-- card-body end .// -->
<div class="border-top card-body text-center"><fmt:message bundle="${rb}" key="registration.account"/>
 <a href="guest.Sing"><fmt:message bundle="${rb}" key="page.home.head.buttonEnter"/></a></div>
</div> <!-- card.// -->
</div> <!-- col.//-->

</div> <!-- row.//-->
</body>
</html>
