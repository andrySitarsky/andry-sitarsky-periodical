$(document).ready(function(){
    $('#admin').find(".form-select").on('change', function () {
     $.post("periodical",
               {command: "admin.ChangeRole",
                user_id: $(this).attr('name')},
                function(responseXml) {
                    $('#error_admin').html($(responseXml).html());
                     if($('#error_admin').text().length > 1){$(this).find('option').removeAttr('selected')
                                                        .filter('[value=ADMIN]').prop('selected', true);}
                }
               );
    });
    $('#admin').find("input.form-check-input").on('change', function (){
    $('#email_mes').val($(this).parents("tr").find("button.btn-secondary").text());
    $('#id_user_modal').val($(this).attr('id'));
    $('#is_blocked').val($(this).val());
    $('#adminMessage').modal('show');
    });
    $('#close_top').on('click', function () {closeModal();});
    $('#close_bottom').on('click', function () {closeModal();});
});
function closeModal() {
var id = $('#id_user_modal').val();
$('#'+id).prop("checked",!$('#'+id).prop("checked"));
  }