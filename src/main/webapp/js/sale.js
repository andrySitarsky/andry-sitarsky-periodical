function openSale(id){
$.post("periodical",
        {command: "user.SaleModal", id_periodical_sale: id},
        function(responseXml) {
            $('#saleModal').empty();
            $('#saleModal').html($(responseXml).html());
        });
}
function returnAmount(){
$.post("periodical",
           {command: "user.ReturnAmount",
            date_start: $('#date_start_sale').val(),
            date_end: $('#date_end_sale').val(),
            price: $('#price_sale').val(),
            number: $('#numberSale').val()},
           function(responseXml) {
               $('#amountSale').html($(responseXml).html());
           });
}