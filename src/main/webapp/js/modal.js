$(document).ready(function(){
    $('#createCatalog').on('click', function () {
       getCatalogs('create','');
    });
});

function changeCatalog(type) {
 var catalog0 ="";
     $('#modalSubCatalogs').children().each(function() {
         catalog0 = catalog0 + $(this).find(":selected").val() + "-";});
     if(type == "add") {catalog0 = catalog0 + "1";}
    getCatalogs(type, catalog0);
 }

 function remCatalog() {
      $('#modalSubCatalogs').children().last().remove();
  }

 function getCatalogs(type, path) {
    $.post("periodical",
        {command: "admin.Modal", catalog: path, type_get: type},
        function(responseXml) {
            $('#modalCatalogs').empty();
            $('#modalCatalogs').html($(responseXml).html());
        });
}
 function changeCatalogCard() {
 var catalog0 ="";
      $('#modalSubCatalogs').children().each(function() {
          catalog0 = catalog0 + $(this).find(":selected").val() + "-";});
          catalog0 = catalog0.substring(0, catalog0.length - 1);
    getCatalogsCard(catalog0);
}
 function getCatalogsCard(path) {
    $.post("periodical",
        {command: "admin.ChangeCatalogCard", catalog: path},
        function(responseXml) {
            $('#card_catalog_id').empty();
            $('#card_catalog_id').html($(responseXml).html());
        });
}
