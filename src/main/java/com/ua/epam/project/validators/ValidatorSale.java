package com.ua.epam.project.validators;

import com.ua.epam.project.beans.Sale;
import com.ua.epam.project.beans.User;

import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import java.time.Period;

public class ValidatorSale {
    public  String getError(Sale sale, User user, String price, LocalizationContext locale){
        return new StringBuilder().append(getErrorDate(sale, locale))
                .append(getErrorAmount(sale, user, price, locale)).toString();
    }
    public String getErrorDate(Sale sale, LocalizationContext locale){
       return (sale.getDateStart().isAfter(sale.getDateEnd()) ? locale.getResourceBundle()
               .getString("validator.dateEnd") : "");
    }

    public String getErrorAmount(Sale sale, User user, String price, LocalizationContext locale) {
        double priceSale = Double.parseDouble(price);
        int month = Period.between(sale.getDateStart(),sale.getDateEnd()).getMonths();
        double sum = priceSale * sale.getNumber() * month;
        double different = sum - user.getBalance().doubleValue();
        return different > 0 ? locale.getResourceBundle().getString("validator.difference") : "";
    }
}
