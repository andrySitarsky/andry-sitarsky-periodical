package com.ua.epam.project.validators;

import com.ua.epam.project.beans.Price;
import com.ua.epam.project.beans.User;
import com.ua.epam.project.beans.decorator.DecorUser;
import com.ua.epam.project.enums.Role;

import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import java.util.List;

public class ValidatorCountAdmin {
    public String getError(List<DecorUser> prices, LocalizationContext locale){
        long count = prices.stream().filter(user -> user.getUser().getRole() == Role.ADMIN).count();
        return (count < 2 ? locale.getResourceBundle().getString("admin.error") : "");
    }
}
