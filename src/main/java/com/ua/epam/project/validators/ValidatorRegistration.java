package com.ua.epam.project.validators;

import com.ua.epam.project.serves.UserServes;

import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import java.time.LocalDate;

public class ValidatorRegistration {
    private UserServes serves;

    public ValidatorRegistration(UserServes serves) {
        this.serves = serves;
    }

    public String getBirth(String birth, LocalDate date, LocalizationContext locale) {
        LocalDate dateBirth = LocalDate.parse(birth);
        LocalDate before18 = date.minusYears(18);
        boolean is18 = before18.isBefore(dateBirth);
        return (is18 ? locale.getResourceBundle().getString("validator.year18") : "");
    }

    public String getEmail(String email, LocalizationContext locale) {
        String result;
        String regex = "\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b";
        long count = serves.getUsers().stream().filter(e -> e.getUser().getEmail().equals(email)).count();
        if (!email.matches(regex)){result = locale.getResourceBundle().getString("validator.emailPath");
        } else if (count > 0){result = locale.getResourceBundle().getString("validator.emailDuplication");
        } else result = "";

        return result;
    }

    public String getLogin(String login, LocalizationContext locale) {
        long count = serves.getUsers().stream().filter(e -> e.getUser().getLogin().equals(login)).count();
        return count >0 ? locale.getResourceBundle().getString("validator.login") : "";
    }
}
