package com.ua.epam.project.validators;

import com.ua.epam.project.beans.Price;

import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import java.util.List;

public class ValidatorPriceCard {
    public String getErrors(List<Price> prices, LocalizationContext locale){
        StringBuilder builder = new StringBuilder();
        boolean isCount= false, isCount1 = false, isCount2 = false;
        for (int i = 0; i < prices.size(); i++) {
            int finalI = i;
            /*Periods intersect*/
            long count = prices.stream().filter(e -> e.getStartData().isAfter(prices.get(finalI).getEndData())).count();
            if (count != (prices.size()-i-1)){isCount = true;}
            /*The gap between periods*/
            long count1 = prices.stream().filter(e -> e.getStartData().compareTo(prices.get(finalI).getEndData().plusDays(1)) == 0).count();
            if ((prices.size()- 1) > i && count1 != 1){isCount1 = true;}
            /*Negative amount*/
            long count2 = prices.stream().filter(e -> e.getPrice().intValue() < 0).count();
            if (count2 > 0){isCount2 = true;}
//            prices.stream().filter(e -> e.getStartData().);
        }
        if (isCount) builder.append(locale.getResourceBundle().getString("validator.dateEnd")).append("<br>");
        if (isCount1) builder.append(locale.getResourceBundle().getString("validator.gap")).append("<br>");
        if (isCount2) builder.append(locale.getResourceBundle().getString("validator.negative"));
        return builder.toString();
    }
}
