package com.ua.epam.project.tld;
import com.ua.epam.project.interfaces.Menu;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import javax.servlet.jsp.tagext.TagSupport;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AccordionMenu extends TagSupport {
    private List<Menu> list;
    private String topic;
    private LocalizationContext bundle;
    private boolean edit;

    public void setList(List<Menu> list) {
        this.list = list;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void setBundle(LocalizationContext bundle) {
        this.bundle = bundle;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            out.write(printJSPMenu());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return SKIP_BODY;
    }
    private String printJSPMenu (){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            factory.setNamespaceAware(true);
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        Document document = builder.newDocument();
        buildDocument(document);
        Transformer transformer = null;
        try {
            transformer = TransformerFactory.newInstance().newTransformer();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }
        transformer.setOutputProperty(OutputKeys.INDENT,"yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        DOMSource source = new DOMSource(document);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        try {
            transformer.transform(source,result);
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        return subDoc(writer.toString());
    }
    /**Delete <?xml version="1.0" encoding="UTF-8" standalone="no"?> */
    private String subDoc(String doc) {
        int position = doc.indexOf(">");
        return doc.substring(position+1);
    }

    private void buildDocument(Document document) {
        int startLength = 1;
        list.sort(Comparator.comparing(Menu::getLevel));
        Element root = document.createElement("div");
        root.setAttribute("class", "accordion");
        root.setAttribute("id", "item0");
        buildElement(document,root,startLength, "");
        document.appendChild(root);
    }

    private void buildElement(Document document, Element root, int startLength, String parent) {
        List<Menu> children = list.stream().filter(e -> e.getLevel().length() == startLength &&
                e.getLevel().startsWith(parent)).collect(Collectors.toList());
        if (children.isEmpty() && startLength == 1) return;
        children.forEach((Menu e) -> {
            long count = list.stream().filter(el -> el.getLevel().startsWith(e.getLevel())).count();
            if (count > 1){
                buildFolder(document, e, root, startLength);
            }else {
                buildSimplElement(document, e, root);
            }

        });

    }


    private void buildFolder(Document document, Menu catalog, Element root, int startLength) {
        Element divRoot = document.createElement("div");
        divRoot.setAttribute("class", "accordion-item ps-0");

        Element h2 = document.createElement("h2");
        h2.setAttribute("class", "accordion-header");
        h2.setAttribute("id","heading"+catalog.getLevel());
        Element button = document.createElement("button");
        button.setAttribute("class","accordion-button collapsed");
        button.setAttribute("type","button");
        button.setAttribute("data-bs-toggle","collapse");
        button.setAttribute("data-bs-target","#item"+catalog.getLevel());
        button.setAttribute("aria-expanded", String.valueOf(topic.startsWith(catalog.getLevel())));
        button.setAttribute("aria-controls","item"+catalog.getLevel());
        if (edit){
        buidSpanButton(document, catalog, button);}
        Element divButton = document.createElement("div");
        divButton.setAttribute("class","col-8");
        if (bundle.getLocale().getLanguage().equalsIgnoreCase("ua")){
        divButton.setTextContent(catalog.getUaName());
        } else {divButton.setTextContent(catalog.getEnName());}
        button.appendChild(divButton);
        if (edit){
        Element span2 = document.createElement("span");
        span2.setAttribute("class","form-check col-1");
        Element inputCheck = document.createElement("input");
        inputCheck.setAttribute("class","form-check-input");
        inputCheck.setAttribute("type","checkbox");
        inputCheck.setAttribute("name","option"+catalog.getId());
        span2.appendChild(inputCheck);
        button.appendChild(span2);}
        h2.appendChild(button);
        divRoot.appendChild(h2);

        Element divNext = document.createElement("div");
        divNext.setAttribute("id","item"+catalog.getLevel());
        divNext.setAttribute("class","accordion-collapse collapse"+
                (topic.startsWith(catalog.getLevel()) ? "show" : ""));
        divNext.setAttribute("aria-labelledby","heading"+catalog.getLevel());
        int length = catalog.getLevel().length();
        divNext.setAttribute("data-bs-parent","#item"+
                (length == 1 ? "0" :catalog.getLevel().substring(0,length-2)));
        Element divChild = document.createElement("div");
        divChild.setAttribute("class","accordion-body");
        Element ul = document.createElement("ul");
        ul.setAttribute("class","btn-toggle-nav list-unstyled fw-normal ps-0");

        createLi(document,catalog,ul,true);

        buildElement(document,ul,startLength + 2, catalog.getLevel());
        divChild.appendChild(ul);
        divNext.appendChild(divChild);
        divRoot.appendChild(divNext);
        root.appendChild(divRoot);
    }

    private void buidSpanButton(Document document, Menu catalog, Element button) {
        Element divSpan = document.createElement("div");
        divSpan.setAttribute("class", "col-2");
        Element span1 = document.createElement("span");
        span1.setAttribute("class", "btn btn-primary");
        span1.setAttribute("data-bs-toggle", "modal");
        span1.setAttribute("data-bs-target", "#myModal");
        span1.setAttribute("onclick", "getCatalogs('edit','" + catalog.getLevel() + "')");
        span1.setTextContent("+");
        divSpan.appendChild(span1);
        button.appendChild(divSpan);
    }

    /**Create div. Put on span1, span2, li  in it. Put on div in root*/
    private void buildSimplElement(Document document, Menu catalog, Element root) {
        Element child = document.createElement("div");
        child.setAttribute("class", "row");
        if (edit){
            buidSpanButton(document, catalog, child);
        }
        createLi(document, catalog, child, false);
        if (edit){
            Element span2 = document.createElement("span");
            span2.setAttribute("class", "form-check col-2");
            Element input = document.createElement("input");
            input.setAttribute("class","form-check-input");
            input.setAttribute("type","checkbox");
            input.setAttribute("name", "option"+ catalog.getId());
            span2.appendChild(input);
            child.appendChild(span2);
        }
        root.appendChild(child);
    }

    private void createLi(Document document, Menu catalog, Element child, boolean all) {
        Element li = document.createElement("li");
        li.setAttribute("class","col");
        Element a = document.createElement("a");
        a.setAttribute("href","periodical?command=home.FindCod&id_topic="+ catalog.getId()+"&cod_topic="+ catalog.getLevel());
        StringBuilder param = new StringBuilder().append("d-inline-flex text-decoration-none rounded");
        if (catalog.getLevel().equals(topic)){param.append("link-red ");
        } else {param.append("link-dark");}
        a.setAttribute("class", param.toString());
        a.setTextContent((all ? bundle.getResourceBundle().getString("menu.all") : "") + " " +
                (bundle.getLocale().getLanguage().equalsIgnoreCase("ua") ? catalog.getUaName()
                        : catalog.getEnName()));
        li.appendChild(a);
        child.appendChild(li);
    }

}
