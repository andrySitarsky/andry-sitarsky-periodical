package com.ua.epam.project.tld;

import com.ua.epam.project.interfaces.Menu;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class ItemMenu extends TagSupport {
    private List<Menu> list;
    private String topic;
    private LocalizationContext bundle;

    public void setList(List<Menu> list) {
        this.list = list;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void setBundle(LocalizationContext bundle) {
        this.bundle = bundle;
    }

    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        String[] folders = topic.split("-");
        StringBuilder builder = new StringBuilder();
        try {
            for (int i = 0; i < folders.length; i++) {
                builder.append(folders[i]);
                Menu menu = list.stream().filter(e -> e.getLevel().equals(builder.toString())).findAny().get();
                if (bundle.getLocale().getLanguage().equalsIgnoreCase("ua")) {
                    out.write(menu.getUaName());
                } else {
                    out.write(menu.getEnName());
                }
                if (i != folders.length - 1) out.write(" -> ");
                builder.append("-");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SKIP_BODY;
    }
}
