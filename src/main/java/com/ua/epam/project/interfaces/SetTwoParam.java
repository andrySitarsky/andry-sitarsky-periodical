package com.ua.epam.project.interfaces;

public interface SetTwoParam <T,V>{
    void setParam(T t, V v);
}
