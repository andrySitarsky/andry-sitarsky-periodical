package com.ua.epam.project.interfaces;

public interface GetParameter <T,V,S>{
    T getParam(V v, S s);
}
