package com.ua.epam.project.interfaces;

public interface SetParameter <T,V,K>{
    void setParam(T t, V v, K k);
}
