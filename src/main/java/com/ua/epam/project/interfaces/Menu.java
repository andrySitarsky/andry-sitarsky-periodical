package com.ua.epam.project.interfaces;

public interface Menu {
    int getId();
    String getLevel();
    String getUaName();
    String getEnName();
}
