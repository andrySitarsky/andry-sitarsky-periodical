package com.ua.epam.project.controllers.commands.guest;

import com.ua.epam.project.beans.User;
import com.ua.epam.project.beans.UserInfo;
import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Gender;
import com.ua.epam.project.enums.Pages;
import com.ua.epam.project.enums.Role;
import com.ua.epam.project.enums.Serves;
import com.ua.epam.project.interfaces.GetParameter;
import com.ua.epam.project.interfaces.SetParameter;
import com.ua.epam.project.serves.UserServes;
import com.ua.epam.project.validators.ValidatorRegistration;

import javax.servlet.ServletException;
import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import java.io.IOException;
import java.time.LocalDate;

public class WriteRegistrationCommand extends FrontCommand {
    private static ValidatorRegistration validator;
    @Override
    public void process() throws ServletException, IOException {
        boolean isSucsses = true;
        if (validator == null){validator = new ValidatorRegistration((UserServes) app.getAttribute(
                Serves.USER_SERVES.name().toLowerCase()));}
        UserServes controller = (UserServes) app.getAttribute(Serves.USER_SERVES.name().toLowerCase());
        LocalizationContext locale = (LocalizationContext) session.getAttribute("rb");
        String adminId = req.getParameter("admin_id");
        int countParam = 0;
        String typeOperation = (String) session.getAttribute("type_operation");
        if (typeOperation != null) {
            User user = new User();
            UserInfo userInfo = new UserInfo();
            User oldUser = null;
            UserInfo oldUserInfo = null;
            if (typeOperation.equals("edit")) {
                if (adminId != null && session.getAttribute("role") == Role.ADMIN.name()){
                    int id = Integer.parseInt(adminId);
                    oldUser = controller.getUser(id);
                    oldUserInfo = controller.getUserInfo(id);
                } else {
                oldUser = (User) session.getAttribute("user");
                oldUserInfo = (UserInfo) session.getAttribute("userInfo");}
            }
            for (Param param : Param.values()) {
                String strParam = req.getParameter(param.name().toLowerCase());
                if (strParam != null && !strParam.isEmpty()) {
                    if (param == Param.PASSWORD){strParam = controller.getHashPassword(strParam);}
                    String message = param.getValidate(strParam, locale);
                    if (!message.isEmpty()){
                        req.setAttribute("error_"+param.name().toLowerCase(), message);
                        countParam--;
                    }
                    param.setParam(user, userInfo, strParam);
                    countParam++;
                } else {
                    if (typeOperation.equals("edit")) {
                        param.setParam(user, userInfo, param.getParam(oldUser, oldUserInfo));
                    }
                }
            }
            if (typeOperation.equals("add") && countParam == Param.values().length){
                user.buildRole(Role.USER);
                controller.addUser(user, userInfo);
                session.setAttribute("role", Role.USER);
            } else if (typeOperation.equals("edit")){
                user.buildRole(Role.USER).buildId(oldUser.getId()).buildBalance(oldUser.getBalance());
                userInfo.buildTextBlocked(oldUserInfo.getTextBlocked()).buildId(oldUser.getId());
                controller.editUser(user, userInfo);
            } else {isSucsses = false;}
            if (adminId == null){
            session.setAttribute("user", user);
            session.setAttribute("userInfo", userInfo);}
        }
        if (isSucsses){redirectCommand(Pages.HOME.getCommand());
        } else {forward(Pages.REGISTRATION.getPath());}
    }


    enum Param {
        FIRST_NAME((user, userInfo, s) -> userInfo.buildFirstName(s), (user, userInfo) -> userInfo.getFirstName(),
                (birth , date) -> ""),
        LAST_NAME((user, userInfo, s) -> userInfo.buildLastName(s), (user, userInfo) -> userInfo.getLastName(),
                (birth , date) -> ""),
        NICK_NAME((user, userInfo, s) -> user.buildNickName(s), (user, userInfo) -> user.getNickName(),
                (birth , date) -> ""),
        CITY((user, userInfo, s) -> userInfo.buildCity(s), (user, userInfo) -> userInfo.getCity(),
                (birth , date) -> ""),
        DATE_BIRTH((user, userInfo, s) -> userInfo.buildDate(LocalDate.parse(s)),
                (user, userInfo) -> userInfo.getDate().toString(),
                (birth , locale) -> validator.getBirth(birth, LocalDate.now(), locale)),
        EMAIL((user, userInfo, s) -> user.buildEmail(s), (user, userInfo) -> user.getEmail(),
                (email , locale) -> validator.getEmail(email, locale)),
        GENDER((user, userInfo, s) -> userInfo.buildGender(Gender.valueOf(s.toUpperCase())),
                (user, userInfo) -> userInfo.getGender().name().toLowerCase(),
                (birth , date) -> ""),
        LOGIN((user, userInfo, s) -> user.buildLogin(s), (user, userInfo) -> user.getLogin(),
                (login , locale) -> validator.getLogin(login, locale)),
        PASSWORD((user, userInfo, s) -> user.buildPassword(s), (user, userInfo) -> user.getPassword(),
                (birth , date) -> "");

        private final SetParameter<User, UserInfo, String> setParameter;
        private final GetParameter<String, User, UserInfo> getParam;
        private final GetParameter<String, String, LocalizationContext> getValidate;


        Param(SetParameter<User, UserInfo, String> param, GetParameter<String, User, UserInfo> getParam,
              GetParameter<String, String, LocalizationContext> getValidate) {
            this.setParameter = param;
            this.getParam = getParam;
            this.getValidate = getValidate;
        }

        public void setParam(User user, UserInfo userInfo, String s) {
            setParameter.setParam(user, userInfo, s);
        }

        public String getParam(User user, UserInfo userInfo) {
            return getParam.getParam(user, userInfo);
        }

        public String getValidate(String s, LocalizationContext locale) {return getValidate.getParam(s, locale);}
    }
}
