package com.ua.epam.project.controllers;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public abstract class FrontCommand {
    protected ServletContext app;
    protected HttpServletRequest req;
    protected HttpServletResponse resp;
    protected HttpSession session;

    public void init(
            ServletContext servletContext,
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        this.app = servletContext;
        this.req = servletRequest;
        this.resp = servletResponse;
        session= req.getSession();
    }

    public abstract void process() throws ServletException, IOException;

    protected void forward(String target) throws ServletException, IOException {
        target = String.format("/pages/%s.jsp", target);
        RequestDispatcher dispatcher = app.getRequestDispatcher(target);
        dispatcher.forward(req, resp);
    }

    protected  void redirectCommand(String target) throws IOException {
        resp.sendRedirect(target);
    }
}
