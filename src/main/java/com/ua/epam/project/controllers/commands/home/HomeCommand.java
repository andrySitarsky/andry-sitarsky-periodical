package com.ua.epam.project.controllers.commands.home;

import javax.servlet.ServletException;
import java.io.IOException;

public class HomeCommand extends FormPageHomeCommand{
    @Override
    public void process() throws ServletException, IOException {
        form();
    }
}
