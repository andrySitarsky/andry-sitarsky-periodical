package com.ua.epam.project.controllers.commands.user;

import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Pages;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ExitUserCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        session.invalidate();
        redirectCommand(Pages.HOME.getCommand());
    }
}
