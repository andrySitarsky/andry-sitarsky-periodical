package com.ua.epam.project.controllers.commands.admin;

import com.ua.epam.project.beans.decorator.DecorUser;
import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Pages;
import com.ua.epam.project.enums.Serves;
import com.ua.epam.project.serves.MailSender;
import com.ua.epam.project.serves.UserServes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

public class MessageCommand extends FrontCommand {
    private Logger log = LogManager.getLogger(MessageCommand.class);
    @Override
    public void process() throws ServletException, IOException {
        String id_user = req.getParameter("id_user");
        String email_message = req.getParameter("email_message");
        String text_info = req.getParameter("text_info");
        Properties prop = new Properties();
        try {
            InputStream stream = MessageCommand.class.getResourceAsStream("/mail.properties");
            prop.load(stream);
        } catch (IOException e) {
            log.error(e);
        }
        String subject;
        if (!id_user.isEmpty()){
            subject = "Повідомлення про блокування";
            int id = Integer.parseInt(id_user);
            UserServes userServes = (UserServes) app.getAttribute(Serves.USER_SERVES.name().toLowerCase());
            List<DecorUser> list = (List<DecorUser>) session.getAttribute("users");
            boolean isBlocked = Boolean.parseBoolean(req.getParameter("is_blocked"));
            userServes.setBlocked(id, text_info, isBlocked);
        } else{ subject = "Повідомлення";}
        MailSender sender = new MailSender(email_message, subject, text_info, prop);
        sender.send();
        redirectCommand(Pages.ADMIN.getCommand());
    }
}
