package com.ua.epam.project.controllers.commands.admin;

import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Pages;
import com.ua.epam.project.enums.Serves;
import com.ua.epam.project.serves.UserServes;

import javax.servlet.ServletException;
import java.io.IOException;

public class AdminPageCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        UserServes serves = (UserServes) app.getAttribute(Serves.USER_SERVES.name().toLowerCase());
        session.setAttribute("users", serves.getUsers());
        forward(Pages.ADMIN.getPath());
    }
}
