package com.ua.epam.project.controllers.commands.home;

import javax.servlet.ServletException;
import java.io.IOException;

public class TextFindCommand extends FormPageHomeCommand{
    @Override
    public void process() throws ServletException, IOException {
        session.setAttribute(HomeParameter.TEXT_FIND.name().toLowerCase(),
                req.getParameter(HomeParameter.TEXT_FIND.name().toLowerCase()));
        if (session.getAttribute(HomeParameter.COD_TOPIC.name().toLowerCase()) != null){
            session.removeAttribute(HomeParameter.COD_TOPIC.name().toLowerCase());
            session.removeAttribute(HomeParameter.ID_TOPIC.name().toLowerCase());
        }
        goPageOne();
        form();
    }
}
