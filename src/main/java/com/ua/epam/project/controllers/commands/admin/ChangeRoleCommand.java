package com.ua.epam.project.controllers.commands.admin;

import com.ua.epam.project.beans.User;
import com.ua.epam.project.beans.decorator.DecorUser;
import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Pages;
import com.ua.epam.project.enums.Role;
import com.ua.epam.project.enums.Serves;
import com.ua.epam.project.serves.UserServes;
import com.ua.epam.project.validators.ValidatorCountAdmin;

import javax.servlet.ServletException;
import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import java.io.IOException;
import java.util.List;

public class ChangeRoleCommand extends FrontCommand {
    private ValidatorCountAdmin validator;
    @Override
    public void process() throws ServletException, IOException {
        if (validator == null){validator = new ValidatorCountAdmin();}
        LocalizationContext locale = (LocalizationContext) session.getAttribute("rb");
        String user_id = req.getParameter("user_id");
        User user = (User) session.getAttribute("user");
        if (user_id != null && user.getRole() == Role.ADMIN){
            UserServes serves = (UserServes) app.getAttribute(Serves.USER_SERVES.name().toLowerCase());
            int id = Integer.parseInt(user_id);
            Role role = serves.getUser(id).getRole();
            if (role == Role.ADMIN){
            List<DecorUser> users = serves.getUsers();
            String error = validator.getError(users,locale);
            if (!error.isEmpty()){
                req.setAttribute("message_admin", error);
            } else {serves.changeRole(id, Role.USER);}
            } else {serves.changeRole(id, Role.ADMIN);}
        }
        forward(Pages.ADMIN_ERROR.getPath());

    }
}
