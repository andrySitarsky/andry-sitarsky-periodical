package com.ua.epam.project.controllers.commands.home;

public enum HomeParameter { TEXT_FIND, ID_TOPIC, COD_TOPIC, PAGE_NUMBER, PAGE_ITEM,
    NAME_TOGGLE, PRICE_TOGGLE, EDIT_PAGE;
}
