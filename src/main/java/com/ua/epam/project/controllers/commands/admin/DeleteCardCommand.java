package com.ua.epam.project.controllers.commands.admin;

import com.ua.epam.project.beans.Periodical;
import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Pages;
import com.ua.epam.project.enums.Serves;
import com.ua.epam.project.serves.CardServes;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DeleteCardCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        int numberPage = (int) session.getAttribute("page_number_start");
        CardServes cardServes = (CardServes) app.getAttribute(Serves.CARD_SERVES.name().toLowerCase());
        List<Periodical> periodicals = new ArrayList<>();
        for (int i = 0; i < numberPage; i++) {
            if (req.getParameter("deletePeriodical" + i) != null){
                periodicals.add(new Periodical().buildId(Integer.parseInt(
                        (String) req.getParameter("deletePeriodical" + i))));
            }
        }
        cardServes.deletePeriodicals(periodicals);
        redirectCommand(Pages.HOME.getCommand());
    }
}
