package com.ua.epam.project.controllers.commands.guest;

import com.ua.epam.project.beans.Catalog;
import com.ua.epam.project.beans.Periodical;
import com.ua.epam.project.beans.Price;
import com.ua.epam.project.beans.decorator.DecorCatalog;
import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Pages;
import com.ua.epam.project.enums.Serves;
import com.ua.epam.project.serves.CardServes;
import com.ua.epam.project.serves.CatalogServes;

import javax.servlet.ServletException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CardCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        mainProcess();
        session.setAttribute("type_page", "card");
        forward(Pages.CARD.getPath());
    }

    private void mainProcess() throws IOException {
        String id = req.getParameter("id");
        String type = req.getParameter("type_operation");
        CardServes controller = (CardServes) app.getAttribute("card_serves");
        CatalogServes catalogServes = (CatalogServes) app.getAttribute(Serves.CATALOG_SERVES.name().toLowerCase());
        if (type != null) {session.setAttribute("type_operation",type);
        } else {if (session.getAttribute("type_operation") != null)
            session.removeAttribute("type_operation");}

        int param = 0;
        List<Catalog> catalogs = null;
        List<Price> prices = null;
        Periodical periodical = null;
        List<DecorCatalog> decorCatalogs = null;
        if (id != null){
            param = Integer.parseInt(id);

            catalogs = controller.getCatalogs(param);
            prices = controller.getPrices(param);
            if (type != null && session.getAttribute("oldCatalogsCard") == null){
                ByteArrayOutputStream baos = controller.getCloneCatalog(catalogs);
                session.setAttribute("oldCatalogsCard", baos);
            }
            periodical = controller.getPeriodical(param);
            decorCatalogs = catalogServes.getCatalogs(catalogs);

        } else {
            periodical = new Periodical();
            decorCatalogs = new ArrayList();
            prices = new ArrayList();
        }
        session.setAttribute("card_id", param);
        session.setAttribute("card", periodical);
        session.setAttribute("card_catalogs", decorCatalogs);
        session.setAttribute("count_catalogs", decorCatalogs.size());
        session.setAttribute("card_data", prices);
        session.setAttribute("count_prices", prices.size());
    }
}
