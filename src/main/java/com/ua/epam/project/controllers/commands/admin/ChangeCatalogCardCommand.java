package com.ua.epam.project.controllers.commands.admin;

import com.ua.epam.project.beans.Catalog;
import com.ua.epam.project.beans.decorator.DecorCatalog;
import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Pages;
import com.ua.epam.project.enums.Serves;
import com.ua.epam.project.serves.CatalogServes;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ChangeCatalogCardCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        int numberNesting = 3;
        String path = req.getParameter("catalog");
        CatalogServes controllerCatalog = (CatalogServes) app.getAttribute(Serves.CATALOG_SERVES.name().toLowerCase());
        List<DecorCatalog> catalogs = (List<DecorCatalog>)session.getAttribute("card_catalogs");
        if (catalogs == null) {
            catalogs = new ArrayList<>();
            session.setAttribute("card_catalogs", catalogs);        }

        if ( path.isEmpty()) {//return old catalogs
        }else if (!catalogs.isEmpty() && path.equals("0")){//delete last catalog
            catalogs.remove(catalogs.size()-1);
        } else if (path.equals("-1")){//add new catalog
            DecorCatalog catalog = controllerCatalog.getCatalog(controllerCatalog.getCatalog("1").getId());
            catalogs.add(controllerCatalog.getCatalog(catalog.getId()));
        }
         else {//edit catalog
            Catalog newCatalog = controllerCatalog.getCatalog(path);
            int id = Integer.parseInt(String.valueOf(session.getAttribute("oldId")));
            if (newCatalog.getId() != id){
                DecorCatalog decorCatalog = controllerCatalog.getCatalog(id);
                int indexOld = catalogs.indexOf(decorCatalog);
                catalogs.set(indexOld, controllerCatalog.getCatalog(newCatalog.getId()));
            }}

        session.setAttribute("count_catalogs", catalogs.size());
        forward(Pages.CARD_CATALOG.getPath());
    }
}
