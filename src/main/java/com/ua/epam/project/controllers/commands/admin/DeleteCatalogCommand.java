package com.ua.epam.project.controllers.commands.admin;

import com.ua.epam.project.beans.Catalog;
import com.ua.epam.project.beans.decorator.DecorCatalog;
import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Pages;
import com.ua.epam.project.enums.Serves;
import com.ua.epam.project.serves.CatalogServes;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.List;

public class DeleteCatalogCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        CatalogServes controller = (CatalogServes) app.getAttribute(Serves.CATALOG_SERVES.name().toLowerCase());
        String delete = req.getParameter("deleteCatalog");
        if (delete != null){
            controller.delete(req);
        }
        redirectCommand(Pages.HOME.getCommand());
    }
}
