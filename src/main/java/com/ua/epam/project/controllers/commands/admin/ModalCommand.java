package com.ua.epam.project.controllers.commands.admin;

import com.ua.epam.project.beans.Catalog;
import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Pages;
import com.ua.epam.project.enums.Serves;
import com.ua.epam.project.serves.CatalogServes;

import javax.servlet.ServletException;
import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import java.io.IOException;

public class ModalCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        CatalogServes controller = (CatalogServes) req.getServletContext()
                .getAttribute(Serves.CATALOG_SERVES.name().toLowerCase());
        String path = req.getParameter("catalog");
        String type = req.getParameter("type_get");
        LocalizationContext locale = (LocalizationContext) session.getAttribute("rb");
        if (type.equals("edit")) {
            if (session.getAttribute("type_page").equals("home")) {
                req.setAttribute("list_catalogs", controller.getParentsCatalogs(path));
            } else {
                req.setAttribute("list_catalogs", controller.getCatalogs(path));
            }
            Catalog catalog = controller.getCatalog(path);
            session.setAttribute("oldName", locale.getLocale().getLanguage()
                    .equalsIgnoreCase("en") ? catalog.getEnName() : catalog.getUaName());
            session.setAttribute("oldId", catalog.getId());
            session.setAttribute("typeOperation", "edit");
        } else if (type.equals("create")) {
            req.setAttribute("list_catalogs", controller.getCatalogs(path));
            session.removeAttribute("oldName");
            session.removeAttribute("oldId");
            session.setAttribute("typeOperation", "create");
        } else {
            req.setAttribute("list_catalogs", controller.getCatalogs(path));
        }
        req.setAttribute("catalog", req.getParameter("catalog"));
        forward(Pages.MODAL.getPath());
    }
}
