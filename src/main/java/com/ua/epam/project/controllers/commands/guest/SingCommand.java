package com.ua.epam.project.controllers.commands.guest;

import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Pages;

import javax.servlet.ServletException;
import java.io.IOException;

public class SingCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        forward(Pages.SING.getPath());
    }
}
