package com.ua.epam.project.controllers.commands.user;

import com.ua.epam.project.beans.User;
import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Pages;
import com.ua.epam.project.enums.Serves;
import com.ua.epam.project.serves.SaleServes;
import com.ua.epam.project.serves.UserServes;

import javax.servlet.ServletException;
import java.io.IOException;

public class ShowSalesUserCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        String adminId = req.getParameter("admin_id");
        User user;
        if (adminId != null){
            UserServes serves = (UserServes) app.getAttribute(Serves.USER_SERVES.name().toLowerCase());
            user = serves.getUser(Integer.parseInt(adminId));
            req.setAttribute("admin", true);
        }else {
        user = (User) session.getAttribute("user");}
        SaleServes sales = (SaleServes) app.getAttribute(Serves.SALE_SERVES.name().toLowerCase());
        session.setAttribute("sales",sales.getSalesUser(user.getId()));
        forward(Pages.USER.getPath());
    }
}
