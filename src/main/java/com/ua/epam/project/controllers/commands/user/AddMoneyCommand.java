package com.ua.epam.project.controllers.commands.user;

import com.ua.epam.project.beans.User;
import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Pages;
import com.ua.epam.project.serves.UserServes;

import javax.servlet.ServletException;
import java.io.IOException;
import java.math.BigDecimal;

public class AddMoneyCommand extends FrontCommand {

    @Override
    public void process() throws ServletException, IOException {
        mainProcess();
        redirectCommand(Pages.HOME.getCommand());
    }

    private void mainProcess() {
        String addMoney = req.getParameter("addMoney");
        if (addMoney != null) {
            UserServes controller = (UserServes) app.getAttribute("user_serves");
            User user = (User) session.getAttribute("user");
            Double sum = null;
            try {
                sum = Double.parseDouble(addMoney);
                if (sum > 0){
                    user.buildBalance(user.getBalance().add(BigDecimal.valueOf(sum)));
                    controller.addMoney(user);
                    Thread.sleep(100);
                }
            } catch (NumberFormatException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
