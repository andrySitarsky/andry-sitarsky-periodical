package com.ua.epam.project.controllers.commands.user;

import com.ua.epam.project.beans.Periodical;
import com.ua.epam.project.beans.User;
import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Pages;
import com.ua.epam.project.enums.Serves;
import com.ua.epam.project.serves.SaleServes;

import javax.servlet.ServletException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public class SaleModalCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        SaleServes serves = (SaleServes) app.getAttribute(Serves.SALE_SERVES.name().toLowerCase());
        User user = (User) session.getAttribute("user");
        String id_srt = req.getParameter(Param.ID_PERIODICAL.param);
        if (id_srt != null){
            int id = Integer.parseInt(id_srt);
            Periodical periodical = ((List<Periodical>)session.getAttribute("cards")).stream()
                    .filter(e -> e.getId() == id).findAny().get();
            session.setAttribute(Param.ID_PERIODICAL.param, id);
            session.setAttribute(Param.DATE.param, LocalDate.now());
            session.setAttribute(Param.PRICE.param, periodical.getPrice());
            session.setAttribute(Param.NAME_PERIODICAL.param, periodical.getName());
        }
        forward(Pages.SALE_MODAL.getPath());

    }
    enum Param{DATE("now_data_sale"),FROM_DATE("from_data_sale"),TO_DATE("to_data_sale"),
        ID_PERIODICAL("id_periodical_sale"), PRICE("price_sale"), AMOUNT("new_amount_sale"),
        NAME_PERIODICAL("name_periodical");
        String param;

        Param(String param) {
            this.param = param;
        }
    }
}
