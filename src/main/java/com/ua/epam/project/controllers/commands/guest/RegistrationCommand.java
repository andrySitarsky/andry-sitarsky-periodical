package com.ua.epam.project.controllers.commands.guest;

import com.ua.epam.project.beans.User;
import com.ua.epam.project.beans.UserInfo;
import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Pages;
import com.ua.epam.project.enums.Role;
import com.ua.epam.project.enums.Serves;
import com.ua.epam.project.serves.UserServes;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class RegistrationCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        String typeOperation = req.getParameter("type_operation");
        String adminId = req.getParameter("admin_id");//edit page admin
        if (typeOperation != null) {
            if (typeOperation.equals("edit")) {
                UserServes controller = (UserServes) app.getAttribute(Serves.USER_SERVES.name().toLowerCase());
                User user;
                if (adminId != null && session.getAttribute("role") == Role.ADMIN.name()){
                    user = controller.getUser(Integer.parseInt(adminId));
                    req.setAttribute("admin", true);
                    req.setAttribute("admin_id", Integer.parseInt(adminId));
                } else {user = (User) session.getAttribute("user");}
                UserInfo userInfo = controller.getUserInfo(user.getId());
                session.setAttribute("userInfo", userInfo);
            }
            session.setAttribute("type_operation", typeOperation);
            forward(Pages.REGISTRATION.getPath());
        }
    }
}
