package com.ua.epam.project.controllers.commands.admin;

import com.ua.epam.project.beans.Catalog;
import com.ua.epam.project.beans.Periodical;
import com.ua.epam.project.beans.Price;
import com.ua.epam.project.beans.decorator.DecorCatalog;
import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Pages;
import com.ua.epam.project.enums.Serves;
import com.ua.epam.project.serves.CardServes;
import com.ua.epam.project.serves.CatalogServes;
import com.ua.epam.project.validators.ValidatorPriceCard;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@MultipartConfig
public class WriteCardCommand extends FrontCommand {
    private Logger log = LogManager.getLogger(WriteCardCommand.class);
    private ValidatorPriceCard validator;

    @Override
    public void process() throws ServletException, IOException {
        CardServes controller = (CardServes) app.getAttribute(Serves.CARD_SERVES.name().toLowerCase());
        if (validator == null){validator = new ValidatorPriceCard();}
        LocalizationContext locale = (LocalizationContext) session.getAttribute("rb");

        /*Write new image*/
        String newName = req.getParameter("NewNameCard");
        String newInfo = req.getParameter("text_info");
        String typeOperation = (String) session.getAttribute("type_operation");
        Part part = req.getPart("file");
        int id = 0;
        Periodical oldPeriodical;
        Periodical newPeriodical = new Periodical();
        if (typeOperation.equals("edit")) id = (int) session.getAttribute("card_id");
        if (typeOperation.equals("edit")) {
            oldPeriodical = (Periodical) session.getAttribute("card");
        } else {
            oldPeriodical = new Periodical();
        }
        if (part != null) {
            String fileName = part.getSubmittedFileName();
            if (!fileName.isEmpty()) {
                session.setAttribute("file_name", fileName);
                String path = app.getRealPath("assets/" + fileName);
                Files.copy(part.getInputStream(), Paths.get(path), StandardCopyOption.REPLACE_EXISTING);
                if (typeOperation.equals("edit")) {
                    Path oldPath = Paths.get(app.getRealPath(oldPeriodical.getPathImage()));
                    if (Files.exists(oldPath)) Files.delete(oldPath);
                }
                newPeriodical.buildPathImage("assets/" + fileName);
                if (typeOperation.equals("add")) oldPeriodical.buildPathImage("assets/" + fileName);
            }
        }
        if (newPeriodical.getPathImage() == null) {//not image
            if (typeOperation.equals("edit")) {
                newPeriodical.buildPathImage(oldPeriodical.getPathImage());
            } else {
                newPeriodical.buildPathImage("assets/tst_09.png");
            }
        }
        /*Write new name*/
        if (newName != null && !newName.isEmpty() && !newName.equals(oldPeriodical.getName())) {
            newPeriodical.buildName(newName).buildFindName(controller.transformationString(newName));
            if (typeOperation.equals("add")) oldPeriodical.buildName(newName)
                    .buildFindName(controller.transformationString(newName));
        } else {
            if (typeOperation.equals("edit")) newPeriodical.buildName(oldPeriodical.getName())
                    .buildFindName(oldPeriodical.getFindName());
        }

        /*Write new info*/
        if (newInfo != null && !newInfo.isEmpty() && !newInfo.equals(oldPeriodical.getInfo())) {
            newPeriodical.buildInfo(newInfo);
            if (typeOperation.equals("add")) oldPeriodical.buildInfo(newInfo);
        } else {
            if (typeOperation.equals("edit")) newPeriodical.buildInfo(oldPeriodical.getInfo());
        }

        if (typeOperation.equals("edit") && !newPeriodical.equals(oldPeriodical))
            newPeriodical.buildId(oldPeriodical.getId());
        controller.updatePice(newPeriodical);
        if (typeOperation.equals("add") && newPeriodical.equals(oldPeriodical))
            id = controller.addPrice(newPeriodical);

        /*Check catalogs*/
        List<Catalog> oldCatalog;
        if (typeOperation.equals("edit")) {
            oldCatalog =
                    controller.getCatalogs((ByteArrayOutputStream) session.getAttribute("oldCatalogsCard"));
        } else {
            oldCatalog = new ArrayList<>();
        }
        List<DecorCatalog> catalogs = (List<DecorCatalog>) session.getAttribute("card_catalogs");
        CatalogServes catalogServes = (CatalogServes) app.getAttribute(Serves.CATALOG_SERVES.name().toLowerCase());
        List<Catalog> simpleCatalogs = catalogServes.getSimpleCatalogs(catalogs);
        controller.checkCatalogs(oldCatalog, simpleCatalogs, id);

        /*Write new prices*/
        ;
        if (writePrices(controller, typeOperation, id, locale)){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                log.error(e);
            }
            redirectCommand(Pages.CARD.getCommand() + "&id=" + id);
        } else {forward(Pages.CARD.getPath());}
    }

    private boolean writePrices(CardServes controller, String typeOperation, int id, LocalizationContext locale) {
        String stringCountPrice = req.getParameter("count_prices");
        boolean isSuccess = true;
        if (stringCountPrice != null) {
            int countPrices = Integer.parseInt(stringCountPrice);
            List<Price> oldPrices;
            int oldCount = 0;
            if (typeOperation.equals("edit")) {
                oldPrices = (List<Price>) session.getAttribute("card_data");
                /*New code*/
                List<Price> newPrices = new ArrayList<>();
                Price price;
                for (int i = 0; i < countPrices; i++) {
                    if (oldPrices.size() > i){
                        price = getPriceOfRequest(req, oldPrices, newPrices, i);
                        price.buildId(oldPrices.get(i).getId());
                    } else {price = getPriceOfRequest(req, new ArrayList<>(), newPrices, i);}
                    newPrices.add(price);
                }
                String errors = validator.getErrors(newPrices,locale);
                if (errors.isEmpty()){
                    controller.writePrice(newPrices, oldPrices, id);
                } else {
                    req.setAttribute("errorPrice", errors);
                    session.setAttribute("card_data", newPrices);
                    session.setAttribute("count_prices", newPrices.size());
                    isSuccess = false;}
            } else {
                oldPrices = new ArrayList<>();
            }

        } else {
            log.error("not count of data");
        }
        return isSuccess;
    }

    private Price getPriceOfRequest(HttpServletRequest req, List<Price> oldPrices, List<Price> newListPrice, int count) {
        Price price = new Price();;
        String fromData = req.getParameter("from_data" + (count + 1));
        String toData = req.getParameter("to_data" + (count + 1));
        String amountPrice = req.getParameter("new_price" + (count + 1));
            if (!fromData.isEmpty()) {
                price.buildStartData(LocalDate.parse(fromData));
            } else {
                price.buildStartData(oldPrices.get(count).getStartData());
            }
            if (!toData.isEmpty()) {
                price.buildEndData(LocalDate.parse(toData));
            } else {
                price.buildEndData(oldPrices.get(count).getEndData());
            }
            if (!amountPrice.isEmpty()) {
                price.buildPrice(BigDecimal.valueOf(Double.parseDouble(amountPrice)));
            } else {
                price.buildPrice(oldPrices.get(count).getPrice());
            }


        return price;
    }
}
