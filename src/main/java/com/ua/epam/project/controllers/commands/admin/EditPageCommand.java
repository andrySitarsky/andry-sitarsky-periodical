package com.ua.epam.project.controllers.commands.admin;

import com.ua.epam.project.controllers.commands.home.FormPageHomeCommand;
import com.ua.epam.project.controllers.commands.home.HomeParameter;

import javax.servlet.ServletException;
import java.io.IOException;

public class EditPageCommand extends FormPageHomeCommand {
    @Override
    public void process() throws ServletException, IOException {
        if (session.getAttribute(HomeParameter.EDIT_PAGE.name().toLowerCase()) != null){
            session.removeAttribute(HomeParameter.EDIT_PAGE.name().toLowerCase());
        } else {session.setAttribute(HomeParameter.EDIT_PAGE.name().toLowerCase(), true);}
        form();
    }
}
