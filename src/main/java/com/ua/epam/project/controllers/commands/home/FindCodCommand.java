package com.ua.epam.project.controllers.commands.home;

import javax.servlet.ServletException;
import java.io.IOException;

public class FindCodCommand extends FormPageHomeCommand{

    @Override
    public void process() throws ServletException, IOException {
        session.setAttribute(HomeParameter.COD_TOPIC.name().toLowerCase(),
                req.getParameter(HomeParameter.COD_TOPIC.name().toLowerCase()));
        session.setAttribute(HomeParameter.ID_TOPIC.name().toLowerCase(),
                req.getParameter(HomeParameter.ID_TOPIC.name().toLowerCase()));
        if (session.getAttribute(HomeParameter.TEXT_FIND.name().toLowerCase()) != null){
            session.removeAttribute(HomeParameter.TEXT_FIND.name().toLowerCase());
        }
        goPageOne();
        form();
    }
}
