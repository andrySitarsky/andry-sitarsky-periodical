package com.ua.epam.project.controllers.commands.user;

import com.ua.epam.project.beans.Sale;
import com.ua.epam.project.beans.User;
import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.db.Database;
import com.ua.epam.project.enums.Pages;
import com.ua.epam.project.enums.Serves;
import com.ua.epam.project.interfaces.SetTwoParam;
import com.ua.epam.project.serves.SaleServes;
import com.ua.epam.project.serves.UserServes;
import com.ua.epam.project.validators.ValidatorSale;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;

public class WriteSaleCommand extends FrontCommand {
    private Logger log = LogManager.getLogger(WriteSaleCommand.class);
    private ValidatorSale validator;

    @Override
    public void process() throws ServletException, IOException {
        if (validator == null){validator = new ValidatorSale();}
        SaleServes serves = (SaleServes) app.getAttribute(Serves.SALE_SERVES.name().toLowerCase());
        UserServes userServes = (UserServes) app.getAttribute(Serves.USER_SERVES.name().toLowerCase());
        User user = (User) session.getAttribute("user");
        LocalizationContext locale = (LocalizationContext) session.getAttribute("rb");
        Sale sale = new Sale();
        String price = req.getParameter("price_sale");
        try {
            for (Param p : Param.values()) {
                String str = req.getParameter(p.param1);
                if (str != null) {
                    p.setSale(sale,str);
                } else {throw new NumberFormatException("Not parse int or double");}
            }
        } catch (NumberFormatException e) {
            log.error(e);
        }
        sale.buildAmount(serves.getAmount(sale, price));

        String error = validator.getError(sale, user, price, locale);
        if (error.isEmpty()){
        if (user.getBalance().compareTo(sale.getAmount()) >= 0){
           serves.salePeriodical(sale,user, userServes.getUserRepo());
            user.buildBalance(user.getBalance().subtract(sale.getAmount()));
        }
        } else { session.setAttribute("toast_message", error);
            session.setAttribute("toggle_toast","on");}
        redirectCommand(Pages.HOME.getCommand());
    }
    enum Param {DATE("now_data_sale", (e,p) -> e.buildDate(LocalDate.parse(p))),
        FROM_DATE("from_data_sale", (e,p) -> e.buildDateStart(LocalDate.parse(p))),
        TO_DATE("to_data_sale", (e,p) -> e.buildDateEnd(LocalDate.parse(p))),
        ID_PERIODICAL("id_periodical_sale", (e,p) -> e.buildIdPeriodical(Integer.parseInt(p))),
        NUMBER("new_number", (e,p) -> e.buildNumber(Integer.parseInt(p)));
        String param1;
        SetTwoParam<Sale, String> param2;

        Param(String param1, SetTwoParam<Sale, String> param2) {
            this.param1 = param1;
            this.param2 = param2;
        }

        Param(String param) {
            this.param1 = param;
        }
        public void setSale(Sale sale, String str){param2.setParam(sale,str);}
    }
}
