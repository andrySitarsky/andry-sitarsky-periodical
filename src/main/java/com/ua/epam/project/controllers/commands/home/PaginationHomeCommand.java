package com.ua.epam.project.controllers.commands.home;

import com.ua.epam.project.enums.Serves;
import com.ua.epam.project.serves.PaginationServes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import java.io.IOException;

public class PaginationHomeCommand extends FormPageHomeCommand{
    private Logger log = LogManager.getLogger(PaginationHomeCommand.class);
    @Override
    public void process() throws ServletException, IOException {
        LocalizationContext locale = (LocalizationContext) session.getAttribute("rb");
        PaginationServes paginationServes = (PaginationServes) app.getAttribute(Serves.PAGINATION_SERVES.name().toLowerCase());
        String pageStr = req.getParameter(HomeParameter.PAGE_ITEM.name().toLowerCase());
        String numberPageStr = req.getParameter(HomeParameter.PAGE_NUMBER.name().toLowerCase());
        int page = (int) session.getAttribute("page_item_start");
        int numberPage = (int) session.getAttribute("page_number_start");
        if (numberPageStr != null){ //set number page
            numberPage = Integer.parseInt(numberPageStr);
            session.setAttribute(HomeParameter.PAGE_NUMBER.name().toLowerCase(),"" + numberPage );
        }
        Object obj = session.getAttribute("number_find");
        if (obj != null) {
            int countPeriodical = (int) obj;// count of find request
            if (pageStr != null) {
                session.setAttribute("page_item_start", paginationServes.getNumberPage(page, pageStr,
                        locale, countPeriodical, numberPage));
            }
        }
        form();//form page Home
    }
}
