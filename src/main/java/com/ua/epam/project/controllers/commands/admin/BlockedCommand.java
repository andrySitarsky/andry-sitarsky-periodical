package com.ua.epam.project.controllers.commands.admin;

import com.ua.epam.project.beans.decorator.DecorUser;
import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Pages;
import com.ua.epam.project.enums.Serves;
import com.ua.epam.project.serves.MailSender;
import com.ua.epam.project.serves.UserServes;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.List;

public class BlockedCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        String id_user = req.getParameter("id_user");
        String text_info = req.getParameter("text_info");
        if (!id_user.isEmpty()){
            int id = Integer.parseInt(id_user);
            UserServes userServes = (UserServes) app.getAttribute(Serves.USER_SERVES.name().toLowerCase());
            boolean isBlocked = userServes.getUser(id).isBlocked();
            userServes.setBlocked(id, text_info, !isBlocked);
        }
        redirectCommand(Pages.ADMIN.getCommand());
    }
}
