package com.ua.epam.project.controllers.commands.admin;

import com.ua.epam.project.beans.Catalog;
import com.ua.epam.project.beans.decorator.DecorCatalog;
import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Pages;
import com.ua.epam.project.enums.Serves;
import com.ua.epam.project.serves.CatalogServes;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.List;

public class ChangeCatalogCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        ServletContext app = req.getServletContext();
        int numberNesting = 3;
        StringBuilder path = new StringBuilder();
        for (int i = 0; i < numberNesting; i++) {
            if (req.getParameter("catalog"+i) != null){
                path.append(req.getParameter("catalog"+i)).append("-");
            }
        }
        if (!path.toString().isEmpty())path.delete(path.length()-1, path.length());

        CatalogServes controller = (CatalogServes) app.getAttribute(Serves.CATALOG_SERVES.name().toLowerCase());
        String oldIdCatalog = req.getParameter("oldIdCatalog");
        if (oldIdCatalog != null){
            DecorCatalog catalog = controller.getCatalog(Integer.parseInt(oldIdCatalog));
            if (path.toString().isEmpty()){
                catalog.buildLevel(controller.getNewPath()).getCatalog().buildParent(catalog.getId())
                        .buildLevel(1);
            } else {
            DecorCatalog parent = controller.getCatalog(controller.getCatalog(path.toString()).getId());
            if (!parent.equals(catalog)) catalog.buildLevel(controller.getNewPath(parent)).getCatalog()
                    .buildLevel(parent.getCatalog().getLevel()+1).buildParent(parent.getId());
            }
            String oldUaName = req.getParameter("uaNewCatalog");
            if (oldUaName != null && !oldUaName.isEmpty()){catalog.getCatalog().buildUaName(oldUaName);}
            String oldEnName = req.getParameter("enNewCatalog");
            if (oldEnName != null && !oldEnName.isEmpty()){catalog.getCatalog().buildEnName(oldEnName);}
            controller.update(catalog.getCatalog());
        } else {
            DecorCatalog catalog = new DecorCatalog().buildCatalog(new Catalog());
            if (path.toString().isEmpty()){
                catalog.buildLevel(controller.getNewPath()).getCatalog().buildLevel(1);
            } else {
                DecorCatalog parent = controller.getCatalog(controller.getCatalog(path.toString()).getId());
                catalog.buildLevel(controller.getNewPath(parent)).getCatalog().buildParent(parent.getId())
                        .buildLevel(parent.getCatalog().getLevel() + 1);
            }

            String oldUaName = req.getParameter("uaNewCatalog");
            if (oldUaName != null && !oldUaName.isEmpty()){catalog.getCatalog().buildUaName(oldUaName);
            } else {catalog.getCatalog().buildUaName("Not name");}
            String oldEnName = req.getParameter("enNewCatalog");
            if (oldEnName != null && !oldEnName.isEmpty()){catalog.getCatalog().buildEnName(oldEnName);
            } else {catalog.getCatalog().buildEnName("Not name");}

            int newId = controller.add(catalog);
            catalog.getCatalog().buildId(newId);
            if (path.toString().isEmpty()){catalog.getCatalog().buildParent(newId);}
        }
        redirectCommand(Pages.HOME.getCommand());
    }
}
