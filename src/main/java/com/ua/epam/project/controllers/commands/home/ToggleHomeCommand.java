package com.ua.epam.project.controllers.commands.home;

import javax.servlet.ServletException;
import java.io.IOException;

public class ToggleHomeCommand extends FormPageHomeCommand{
    private static final String INCREASE = "increase";
    private static final String DECREASE = "decrease";

    @Override
    public void process() throws ServletException, IOException {
        String nameToggle = req.getParameter(HomeParameter.NAME_TOGGLE.name().toLowerCase());
        if (nameToggle != null) {
            if (session.getAttribute(HomeParameter.PRICE_TOGGLE.name().toLowerCase()) != null) {
                session.removeAttribute(HomeParameter.PRICE_TOGGLE.name().toLowerCase());
            }
            if (nameToggle.equals(INCREASE)) {
                session.setAttribute(HomeParameter.NAME_TOGGLE.name().toLowerCase(), INCREASE);
            } else {
                session.setAttribute(HomeParameter.NAME_TOGGLE.name().toLowerCase(), DECREASE);
            }
        }
        String priceToggle = req.getParameter(HomeParameter.PRICE_TOGGLE.name().toLowerCase());
        if (priceToggle != null) {
            if (session.getAttribute(HomeParameter.NAME_TOGGLE.name().toLowerCase()) != null) {
                session.removeAttribute(HomeParameter.NAME_TOGGLE.name().toLowerCase());
            }
            if (priceToggle.equals(INCREASE)) {
                session.setAttribute(HomeParameter.PRICE_TOGGLE.name().toLowerCase(), INCREASE);
            } else {
                session.setAttribute(HomeParameter.PRICE_TOGGLE.name().toLowerCase(), DECREASE);
            }
        }
        goPageOne();
        form();//form page Home
    }
}
