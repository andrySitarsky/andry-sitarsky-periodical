package com.ua.epam.project.controllers.commands.guest;

import com.ua.epam.project.beans.User;
import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Pages;
import com.ua.epam.project.enums.Role;
import com.ua.epam.project.enums.Serves;
import com.ua.epam.project.serves.UserServes;

import javax.servlet.ServletException;
import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import java.io.IOException;

public class EnterUserCommand extends FrontCommand {

    private static final String INCORRECT_LOGIN = "incorrect_login";


    @Override
    public void process() throws ServletException, IOException {
        UserServes controller = (UserServes) app.getAttribute(Serves.USER_SERVES.name().toLowerCase());
        LocalizationContext locale = (LocalizationContext) session.getAttribute("rb");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        if (login != null && password != null) {
            User user = controller.getUser(login, password);
            if (user != null) {
                if (!user.isBlocked()){
                session.setAttribute("user", user);
                session.setAttribute("role", user.getRole().name());
                if (session.getAttribute(INCORRECT_LOGIN) != null){
                    session.removeAttribute(INCORRECT_LOGIN);
                }
                redirectCommand(Pages.HOME.getCommand());
                } else {
                    session.setAttribute("text_blocked", controller.getUserInfo(user.getId()).getTextBlocked());
                    forward(Pages.BLOCKED.getPath());}
            } else {
                session.setAttribute(INCORRECT_LOGIN, locale.getResourceBundle().getString("sing.incorrectLogin"));
                redirectCommand(Pages.SING.getCommand());
            }
        } else {
            redirectCommand(Pages.SING.getCommand());
        }
    }
}