package com.ua.epam.project.controllers.commands.user;

import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Pages;

import javax.servlet.ServletException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Period;
import java.util.EnumMap;
import java.util.Map;

public class ReturnAmountCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        Map<Param, String> map = new EnumMap<Param, String>(Param.class);
        for (Param p : Param.values()) {
            if (req.getParameter(p.name().toLowerCase()) != null) {
                map.put(p, req.getParameter(p.name().toLowerCase()));
            }
        }
        double result;
        if (map.size() == Param.values().length){
            result = Period.between(LocalDate.parse(map.get(Param.DATE_START)),
                    LocalDate.parse(map.get(Param.DATE_END))).getMonths() *
                    Integer.parseInt(map.get(Param.NUMBER)) *
                    Double.parseDouble(map.get(Param.PRICE));
        } else {result = 0;}
        req.setAttribute("amount_sale", result);
        forward(Pages.AMOUNT.getPath());

    }
    enum Param {DATE_START, DATE_END, NUMBER, PRICE;}
}
