package com.ua.epam.project.controllers.commands.user;

import com.ua.epam.project.controllers.commands.home.FormPageHomeCommand;

import javax.servlet.ServletException;
import java.io.IOException;

public class ToastCommand extends FormPageHomeCommand {
    @Override
    public void process() throws ServletException, IOException {
        req.setAttribute("toggle_toast","on");
        form();
    }
}
