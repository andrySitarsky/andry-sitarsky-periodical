package com.ua.epam.project.controllers.commands.home;

import com.ua.epam.project.beans.Periodical;
import com.ua.epam.project.beans.decorator.DecorCatalog;
import com.ua.epam.project.controllers.FrontCommand;
import com.ua.epam.project.enums.Pages;
import com.ua.epam.project.enums.Serves;
import com.ua.epam.project.serves.CardServes;
import com.ua.epam.project.serves.CatalogServes;

import javax.servlet.ServletException;
import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class FormPageHomeCommand extends FrontCommand {
    private static final String TYPE_PAGE = "type_page";
    private static final String PAGE = "home";
    private static final String PAGE_ITEM_START = "page_item_start";
    private static final int PAGE_NUMBER = 1;

    protected void form() throws ServletException, IOException {
        LocalizationContext locale = (LocalizationContext) session.getAttribute("rb");
        int page = (int) session.getAttribute("page_item_start");
        int numberPage = (int) session.getAttribute("page_number_start");

        List<Periodical> periodicals;
        CardServes cardServes = (CardServes) app.getAttribute(Serves.CARD_SERVES.name().toLowerCase());
        CatalogServes catalogServes = (CatalogServes) app.getAttribute(Serves.CATALOG_SERVES.name().toLowerCase());
        String nameFind = (String) session.getAttribute(HomeParameter.TEXT_FIND.name().toLowerCase());
        String codeTopic = (String) session.getAttribute(HomeParameter.COD_TOPIC.name().toLowerCase());
        String nameToggle = (String) session.getAttribute("name_toggle");
        String priceToggle = (String) session.getAttribute("price_toggle");

        int start = numberPage * (page - 1);//start card of page
        int countPeriodical = 0;
        if (codeTopic != null) {
            List<DecorCatalog> catalogs = catalogServes.getChildrenCatalog(codeTopic);
            periodicals = cardServes.getPeriodicals(catalogs, nameToggle, priceToggle, numberPage, start);
            countPeriodical = cardServes.getCountPeriodicalOfCatalog(catalogs);
        } else if (nameFind != null) {
            if (nameFind.isEmpty()) {
                periodicals = cardServes.getAll(nameToggle, priceToggle, numberPage, start);
                countPeriodical = cardServes.getCountAll();
            } else {
                String newName = cardServes.transformationString(nameFind);
                periodicals = cardServes.findCardsForName(nameToggle, priceToggle, newName, numberPage, start);
                countPeriodical = cardServes.getCountPeriodicalOfName(newName);
            }
        } else {
            periodicals = new ArrayList<>();
        }
        if (countPeriodical > numberPage) {
            session.setAttribute("toggle_pagination", "on");

            session.setAttribute("pagination", cardServes.getPagination(countPeriodical, numberPage,
                    page, locale));
        } else {
            session.setAttribute("toggle_pagination", "off");
        }
        session.setAttribute("number_find", countPeriodical);
        session.setAttribute("cards", periodicals);
        session.setAttribute(TYPE_PAGE, PAGE);
        forward(Pages.HOME.getPath());
    }
    protected void goPageOne(){
        session.setAttribute(PAGE_ITEM_START, PAGE_NUMBER);
    }
}
