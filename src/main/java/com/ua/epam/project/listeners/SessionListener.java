package com.ua.epam.project.listeners;

import com.ua.epam.project.enums.Role;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import java.util.Locale;
import java.util.ResourceBundle;

@WebListener
public class SessionListener implements HttpSessionListener {

    private static final int COUNT_PAGES = 6;
    private static final int ONE_PAGE = 1;
    private static final String ALL_CARD = "";

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        HttpSession session = se.getSession();
        session.setAttribute("page_number_start", COUNT_PAGES);
        session.setAttribute("page_item_start", ONE_PAGE);
        session.setAttribute("text_find", ALL_CARD);//enter all card
        session.setAttribute("role", Role.GUEST.name());
        session.setAttribute("rb", new LocalizationContext(
                ResourceBundle.getBundle("messages",
                new Locale("ua", "UA"))));
    }
}
