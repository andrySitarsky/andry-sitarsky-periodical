package com.ua.epam.project.listeners;

import com.ua.epam.project.db.Database;
import com.ua.epam.project.enums.Serves;
import com.ua.epam.project.serves.*;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        final ServletContext context = sce.getServletContext();
        Database.instance();
        CatalogServes controller = new CatalogServes();
        controller.createListCatalog();
        context.setAttribute(Serves.CATALOG_SERVES.name().toLowerCase(), controller);
        context.setAttribute(Serves.CARD_SERVES.name().toLowerCase(), new CardServes());
        context.setAttribute(Serves.USER_SERVES.name().toLowerCase(), new UserServes());
        context.setAttribute(Serves.SALE_SERVES.name().toLowerCase(), new SaleServes());
        context.setAttribute(Serves.PAGINATION_SERVES.name().toLowerCase(), new PaginationServes());
    }

}
