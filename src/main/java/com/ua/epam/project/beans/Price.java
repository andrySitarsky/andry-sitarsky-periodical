package com.ua.epam.project.beans;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class Price {
    private int id;
    private LocalDate startData;
    private LocalDate endData;
    private BigDecimal price;

    public BigDecimal getPrice() {
        return price;
    }

    public Price buildPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public int getId() {
        return id;
    }

    public Price buildId(int id) {
        this.id = id;
        return this;
    }

    public LocalDate getStartData() {
        return startData;
    }

    public Price buildStartData(LocalDate startData) {
        this.startData = startData;
        return this;
    }

    public LocalDate getEndData() {
        return endData;
    }

    public Price buildEndData(LocalDate endData) {
        this.endData = endData;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Price price1 = (Price) o;
        return Objects.equals(startData, price1.startData) && Objects.equals(endData, price1.endData) && Objects.equals(price, price1.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startData, endData, price);
    }
}
