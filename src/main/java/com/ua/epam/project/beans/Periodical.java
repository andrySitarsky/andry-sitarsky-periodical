package com.ua.epam.project.beans;

import java.math.BigDecimal;
import java.util.Objects;

public class Periodical {
    private int id;
    private String name;
    private String findName;
    private String pathImage;
    private String info;
    private BigDecimal price;

    public Periodical() {
    }

    public int getId() {
        return id;
    }

    public Periodical buildId(int id) {
        this.id = id;
        return this;
    }

    public String getFindName() {
        return findName;
    }

    public Periodical buildFindName(String findName) {
        this.findName = findName;
        return this;
    }

    public String getName() {
        return name;
    }

    public Periodical buildName(String name) {
        this.name = name;
        return this;
    }


    public String getPathImage() {
        return pathImage;
    }

    public Periodical buildPathImage(String pathImage) {
        this.pathImage = pathImage;
        return this;
    }

    public String getInfo() {
        return info;
    }

    public Periodical buildInfo(String info) {
        this.info = info;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Periodical buildPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Periodical that = (Periodical) o;
        return Objects.equals(name, that.name) && Objects.equals(pathImage, that.pathImage) && Objects.equals(info, that.info);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, pathImage, info);
    }
}
