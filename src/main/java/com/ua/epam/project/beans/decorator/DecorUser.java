package com.ua.epam.project.beans.decorator;

import com.ua.epam.project.beans.User;

import java.math.BigDecimal;

public class DecorUser {
    private User user;
    private int count;
    private BigDecimal sum;

    public User getUser() {
        return user;
    }

    public DecorUser buildUser(User user) {
        this.user = user;
        return this;
    }

    public int getCount() {
        return count;
    }

    public DecorUser buildCount(int count) {
        this.count = count;
        return this;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public DecorUser buildSum(BigDecimal sum) {
        this.sum = sum;
        return this;
    }
}
