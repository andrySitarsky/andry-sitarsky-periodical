package com.ua.epam.project.beans.decorator;

import com.ua.epam.project.beans.Catalog;
import com.ua.epam.project.interfaces.Menu;

import java.io.Serializable;
import java.util.Objects;

public class DecorCatalog implements Serializable, Menu {
    private Catalog catalog;
    public String level;

    public Catalog getCatalog() {
        return catalog;
    }

    public DecorCatalog buildCatalog(Catalog catalog) {
        this.catalog = catalog;
        return this;
    }

    @Override
    public int getId() {
        return catalog.getId();
    }

    public String getLevel() {
        return level;
    }

    @Override
    public String getUaName() {
        return catalog.getUaName();
    }

    @Override
    public String getEnName() {
        return catalog.getEnName();
    }

    public DecorCatalog buildLevel(String level) {
        this.level = level;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DecorCatalog that = (DecorCatalog) o;
        return Objects.equals(catalog, that.catalog);
    }

    @Override
    public int hashCode() {
        return Objects.hash(catalog);
    }
}
