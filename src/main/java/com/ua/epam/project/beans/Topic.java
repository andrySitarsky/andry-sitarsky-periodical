package com.ua.epam.project.beans;

public class Topic {
    private boolean active;
    private String name;

    public Topic(boolean isActive, String name) {
        this.active = isActive;
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public String getName() {
        return name;
    }
}
