package com.ua.epam.project.beans;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Sale {
    private int id;
    private int idUser;
    private int idPeriodical;
    private LocalDate date;
    private String namePeriodical;
    private LocalDate dateStart;
    private LocalDate dateEnd;
    private int number;
    private BigDecimal amount;

    public Sale() {
    }

    public int getIdUser() {
        return idUser;
    }

    public Sale buildIdUser(int idUser) {
        this.idUser = idUser;
        return this;
    }

    public int getIdPeriodical() {
        return idPeriodical;
    }

    public Sale buildIdPeriodical(int idPeriodical) {
        this.idPeriodical = idPeriodical;
        return this;
    }

    public int getId() {
        return id;
    }

    public Sale buildId(int id) {
        this.id = id;
        return this;
    }

    public LocalDate getDate() {
        return date;
    }

    public Sale buildDate(LocalDate date) {
        this.date = date;
        return this;
    }

    public String getNamePeriodical() {
        return namePeriodical;
    }

    public Sale buildNamePeriodical(String namePeriodical) {
        this.namePeriodical = namePeriodical;
        return this;
    }

    public LocalDate getDateStart() {
        return dateStart;
    }

    public Sale buildDateStart(LocalDate dateStart) {
        this.dateStart = dateStart;
        return this;
    }

    public LocalDate getDateEnd() {
        return dateEnd;
    }

    public Sale buildDateEnd(LocalDate dateEnd) {
        this.dateEnd = dateEnd;
        return this;
    }

    public int getNumber() {
        return number;
    }

    public Sale buildNumber(int number) {
        this.number = number;
        return this;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Sale buildAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }
}
