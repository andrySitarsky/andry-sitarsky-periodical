package com.ua.epam.project.beans;

import com.ua.epam.project.enums.Role;

import java.math.BigDecimal;

public class User {
    private int id;
    private Role role;
    private String nickName;
    private String login;
    private String password;
    private String email;
    private BigDecimal balance;
    private boolean isBlocked;

    public boolean isBlocked() {
        return isBlocked;
    }

    public User buildBlocked(boolean blocked) {
        isBlocked = blocked;
        return this;
    }

    public int getId() {
        return id;
    }

    public User buildId(int id) {
        this.id = id;
        return this;
    }

    public Role getRole() {
        return role;
    }

    public User buildRole(Role role) {
        this.role = role; return this;
    }

    public String getNickName() {
        return nickName;
    }

    public User buildNickName(String nickName) {
        this.nickName = nickName; return this;
    }

    public String getLogin() {
        return login;
    }

    public User buildLogin(String login) {
        this.login = login;return this;
    }

    public String getPassword() {
        return password;
    }

    public User buildPassword(String password) {
        this.password = password; return this;
    }

    public String getEmail() {
        return email;
    }

    public User buildEmail(String email) {
        this.email = email;return this;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public User buildBalance(BigDecimal balance) {
        this.balance = balance;return this;
    }


    public User() {
    }
}
