package com.ua.epam.project.beans;

import java.io.Serializable;
import java.util.Objects;

public class  Catalog implements Serializable {
    private static final int STEP_WRITE_CATALOG = 2;
    private int id;
    private int level;
    private String uaName;
    private String enName;
    private int parent;
    private int count;

    public Catalog() {
    }

    public int getCount() {
        return count;
    }

    public Catalog buildCount(int count) {
        this.count = count;
        return this;
    }

    public int getId() {
        return id;
    }

    public Catalog buildId(int id) {
        this.id = id;
        return this;
    }

    public int getParent() {
        return parent;
    }

    public Catalog buildParent(int parent) {
        this.parent = parent;
        return this;
    }

    public int getLevel() {
        return level;
    }

    public Catalog buildLevel(int level) {
        this.level = level;
        return this;
    }

    public String getUaName() {
        return uaName;
    }

    public Catalog buildUaName(String uaName) {
        if (!uaName.isEmpty()) this.uaName = uaName;
        return this;
    }

    public String getEnName() {
        return enName;
    }

    public Catalog buildEnName(String enName) {
        if (!enName.isEmpty()) this.enName = enName;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Catalog catalog = (Catalog) o;
        return id == catalog.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
