package com.ua.epam.project.beans;

import com.ua.epam.project.enums.Gender;

import java.time.LocalDate;

public class UserInfo {
    private int id;
    private String firstName;
    private String lastName;
    private String city;
    private LocalDate date;
    private Gender gender;
    private String textBlocked;

    public UserInfo() {
    }

    public int getId() {
        return id;
    }

    public UserInfo buildId(int id) {
        this.id = id;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public UserInfo buildFirstName(String firstName) {
        this.firstName = firstName; return this;
    }

    public String getLastName() {
        return lastName;
    }

    public UserInfo buildLastName(String lastName) {
        this.lastName = lastName; return this;
    }

    public String getCity() {
        return city;
    }

    public UserInfo buildCity(String city) {
        this.city = city; return this;
    }

    public LocalDate getDate() {
        return date;
    }

    public UserInfo buildDate(LocalDate date) {
        this.date = date; return this;
    }

    public Gender getGender() {
        return gender;
    }

    public UserInfo buildGender(Gender gender) {
        this.gender = gender; return this;
    }

    public String getTextBlocked() {
        return textBlocked;
    }

    public UserInfo buildTextBlocked(String textBlocked) {
        this.textBlocked = textBlocked; return this;
    }
}
