package com.ua.epam.project.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

@WebFilter(urlPatterns = "/*")
public class SessionLocaleFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        String locale = req.getParameter("sessionLocale") ;
        if (locale != null){
            req.getSession().setAttribute("rb", new LocalizationContext(ResourceBundle
                    .getBundle("messages", new Locale(locale.toLowerCase(), locale.toUpperCase()))));
        }
        chain.doFilter(request, response);
    }
}
