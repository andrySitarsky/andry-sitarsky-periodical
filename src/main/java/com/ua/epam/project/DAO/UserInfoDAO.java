package com.ua.epam.project.DAO;

import com.ua.epam.project.beans.UserInfo;
import com.ua.epam.project.enums.Gender;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

public class UserInfoDAO implements DAO<UserInfo, Integer, ResultSet>{
    private Connection conn;
    private Logger log = LogManager.getLogger(UserInfoDAO.class);

    public UserInfoDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public int create(UserInfo userInfo) {
        int id = -1;
        try (PreparedStatement stmt = conn.prepareStatement(SQLUserInfo.INSERT.sql)){
            stmt.setInt(1, userInfo.getId());
            stmt.setString(2, userInfo.getLastName());
            stmt.setString(3, userInfo.getFirstName());
            stmt.setString(4, userInfo.getCity());
            stmt.setDate(5, Date.valueOf(userInfo.getDate()));
            stmt.setString(6, userInfo.getGender().name());
            stmt.setString(7, userInfo.getTextBlocked());
            try (ResultSet rs = stmt.executeQuery()){
                if (rs.next()) id = rs.getInt("id");}
        } catch (SQLException e) {
            log.error(e);
        }
        return id;
    }

    @Override
    public UserInfo read(Integer id) {
        UserInfo userInfo = null;
        try (PreparedStatement stmt = conn.prepareStatement(SQLUserInfo.GET.sql)){
            stmt.setInt(1, id);
            try(ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) userInfo = getResult(rs);
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return userInfo;
    }

    @Override
    public UserInfo getResult(ResultSet rs) throws SQLException {
        return new UserInfo().buildLastName(rs.getString("last_name"))
                .buildFirstName(rs.getString("first_name"))
                .buildCity(rs.getString("city"))
                .buildDate(rs.getDate("birthday").toLocalDate())
                .buildGender(Gender.valueOf(rs.getString("sex")))
                .buildTextBlocked(rs.getString("text_blocked"))
                .buildId(rs.getInt("id"));
    }

    @Override
    public boolean update(UserInfo userInfo) {
        boolean isSuccess= false;
        try (PreparedStatement stmt = conn.prepareStatement(SQLUserInfo.UPDATE.sql)){
            stmt.setString(1, userInfo.getLastName());
            stmt.setString(2, userInfo.getFirstName());
            stmt.setString(3, userInfo.getCity());
            stmt.setDate(4, Date.valueOf(userInfo.getDate()));
            stmt.setString(5, userInfo.getGender().name());
            stmt.setString(6, userInfo.getTextBlocked());
            stmt.setInt(7, userInfo.getId());
            isSuccess = stmt.executeQuery().next();
        } catch (SQLException e) {
            log.error(e);
        }
        return isSuccess;
    }

    @Override
    public boolean delete(UserInfo userInfo) {
        boolean isSuccess= false;
        try (PreparedStatement stmt = conn.prepareStatement(SQLUserInfo.DELETE.sql)){
            stmt.setInt(1, userInfo.getId());
            isSuccess = stmt.executeQuery().next();
        } catch (SQLException e) {
            log.error(e);
        }
        return isSuccess;
    }

    public boolean blockedInfo(int id, String text_info) {
        boolean isSuccess= false;
        try (PreparedStatement stmt = conn.prepareStatement(SQLUserInfo.BLOCKED.sql)){
            stmt.setString(1, text_info);
            stmt.setInt(2, id);
            isSuccess = stmt.executeQuery().next();
        } catch (SQLException e) {
            log.error(e);
        }
        return isSuccess;
    }

    enum SQLUserInfo{
        INSERT("INSERT INTO user_info (id,last_name,first_name,city,birthday,sex,text_blocked) " +
                "VALUES (?,?,?,?,?,?,?) RETURNING id"),
        DELETE("DELETE FROM user_info WHERE id=(?) RETURNING id"),
        UPDATE("UPDATE user_info SET last_name=(?), first_name=(?), city=(?), birthday=(?), sex=(?), " +
                "text_blocked=(?) WHERE id=(?) RETURNING id"),
        GET("SELECT * FROM user_info WHERE id=(?)"),
        BLOCKED("UPDATE user_info SET text_blocked=(?) WHERE id=(?) RETURNING id");
        String sql;

        SQLUserInfo (String sql) {
            this.sql = sql;
        }
    }
}
