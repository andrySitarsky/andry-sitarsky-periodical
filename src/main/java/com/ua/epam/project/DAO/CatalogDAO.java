package com.ua.epam.project.DAO;

import com.ua.epam.project.beans.Catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CatalogDAO implements DAO<Catalog,Integer,ResultSet>{
    private Connection conn;

    public CatalogDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public int create(Catalog catalog) {
        int id = -1;
        try (PreparedStatement stmt = conn.prepareStatement(SQLCatalog.INSERT.sql)){
            stmt.setInt(1, catalog.getLevel());
            stmt.setString(2, catalog.getUaName());
            stmt.setString(3, catalog.getEnName());
            stmt.setInt(4, catalog.getParent());
            try (ResultSet rs = stmt.executeQuery()){
            if (rs.next()) id = rs.getInt("id");}
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    @Override
    public Catalog read(Integer id) {
        Catalog catalog = null;
        try (PreparedStatement stmt = conn.prepareStatement(SQLCatalog.GET.sql)){
            stmt.setInt(1, id);
            try(ResultSet rs = stmt.executeQuery()) {
               catalog = getResult(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return catalog;
    }

    @Override
    public Catalog getResult(ResultSet rs) throws SQLException {
        return new Catalog().buildId(rs.getInt("id"))
                .buildLevel(rs.getInt("level"))
                .buildUaName(rs.getString("name_ua"))
                .buildEnName(rs.getString("name_en"))
                .buildParent(rs.getInt("parent"));
    }

    @Override
    public boolean update(Catalog catalog) {
        boolean isSuccess= false;
        try (PreparedStatement stmt = conn.prepareStatement(SQLCatalog.UPDATE.sql)){
            stmt.setInt(1, catalog.getLevel());
            stmt.setString(2, catalog.getUaName());
            stmt.setString(3, catalog.getEnName());
            stmt.setInt(4, catalog.getParent());
            stmt.setInt(5, catalog.getId());
            isSuccess = stmt.executeQuery().next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isSuccess;
    }

    @Override
    public boolean delete(Catalog catalog) {
        boolean isSuccess= false;
        try (PreparedStatement stmt = conn.prepareStatement(SQLCatalog.DELETE.sql)){
            stmt.setInt(1, catalog.getId());
            isSuccess = stmt.executeQuery().next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isSuccess;
    }
    enum SQLCatalog{
        INSERT("INSERT INTO catalogs (id,level,name_ua,name_en,parent) VALUES(DEFAULT,(?),(?),(?),(?)) RETURNING id"),
        DELETE("DELETE FROM catalogs WHERE id=(?) RETURNING id"),
        UPDATE("UPDATE catalogs SET level=(?), name_ua=(?), name_en=(?), parent=(?) WHERE id=(?) RETURNING id"),
        GET("SELECT * FROM catalogs WHERE id=(?)");
        String sql;
        SQLCatalog(String sql) {
            this.sql = sql;
        }
    }
}
