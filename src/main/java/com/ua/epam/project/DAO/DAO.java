package com.ua.epam.project.DAO;

import java.sql.SQLException;

public interface DAO <Entity,Key,Result>{
    int create(Entity entity);
    Entity read(Key key);
    Entity getResult(Result result) throws SQLException;
    boolean update(Entity entity);
    boolean delete(Entity entity);
}
