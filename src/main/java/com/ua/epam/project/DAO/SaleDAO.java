package com.ua.epam.project.DAO;

import com.ua.epam.project.beans.Sale;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

public class SaleDAO implements DAO<Sale, Integer, ResultSet>{
    private Connection conn;
    private Logger log = LogManager.getLogger(SaleDAO.class);

    public SaleDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public int create(Sale sale) {
        int id = -1;
        try (PreparedStatement stmt = conn.prepareStatement(SQLSale.INSERT.sql)){
            stmt.setDate(1, Date.valueOf(sale.getDate()));
            stmt.setInt(2, sale.getIdUser());
            stmt.setInt(3, sale.getIdPeriodical());
            stmt.setDate(4, Date.valueOf(sale.getDateStart()));
            stmt.setDate(5, Date.valueOf(sale.getDateEnd()));
            stmt.setInt(6, sale.getNumber());
            stmt.setBigDecimal(7, sale.getAmount());
            try (ResultSet rs = stmt.executeQuery()){
                if (rs.next()) id = rs.getInt("id");}
        } catch (SQLException e) {
            log.error(e);
        }
        return id;
    }

    @Override
    public Sale read(Integer id) {
        Sale sale = null;
        try (PreparedStatement stmt = conn.prepareStatement(SQLSale.GET.sql)){
            stmt.setInt(1, id);
            try (ResultSet rs = stmt.executeQuery()){
                if (rs.next()) sale = getResult(rs);
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return sale;
    }

    @Override
    public Sale getResult(ResultSet rs) throws SQLException {
        return new Sale().buildId(rs.getInt("id"))
                .buildIdUser(rs.getInt("id_user"))
                .buildIdPeriodical(rs.getInt("id_periodical"))
                .buildNamePeriodical(rs.getString("name"))
                .buildDate(rs.getDate("date").toLocalDate())
                .buildDateStart(rs.getDate("date_start").toLocalDate())
                .buildDateEnd(rs.getDate("date_end").toLocalDate())
                .buildNumber(rs.getInt("number"))
                .buildAmount(rs.getBigDecimal("amount"));
    }

    @Override
    public boolean update(Sale sale) {
        boolean isSuccess= false;
        try (PreparedStatement stmt = conn.prepareStatement(UserDAO.SQLUser.UPDATE.sql)){
            stmt.setDate(1, Date.valueOf(sale.getDate()));
            stmt.setInt(2, sale.getIdPeriodical());
            stmt.setDate(3, Date.valueOf(sale.getDateStart()));
            stmt.setDate(4, Date.valueOf(sale.getDateEnd()));
            stmt.setInt(5, sale.getNumber());
            stmt.setBigDecimal(6,sale.getAmount());
            stmt.setInt(7, sale.getId());
            isSuccess = stmt.executeQuery().next();
        } catch (SQLException e) {
            log.error(e);
        }
        return isSuccess;
    }

    @Override
    public boolean delete(Sale sale) {
        boolean isSuccess= false;
        try (PreparedStatement stmt = conn.prepareStatement(SQLSale.DELETE.sql)){
            stmt.setInt(1, sale.getId());
            isSuccess = stmt.executeQuery().next();
        } catch (SQLException e) {
            log.error(e);
        }
        return isSuccess;
    }
    enum SQLSale{
        INSERT("INSERT INTO sale (id, date, id_user, id_periodical,date_start, date_end, number, amount)" +
                " VALUES (DEFAULT,?,?,?,?,?,?,?) RETURNING id"),
        DELETE("DELETE FROM sale WHERE id=(?) RETURNING id"),
        UPDATE("UPDATE sale SET date=(?), id_periodical=(?), date_start=(?), date_end=(?), number=(?)," +
                " amount=(?), WHERE id=(?) RETURNING id"),
        GET("SELECT s.*, p.name FROM sale s JOIN periodicals p ON s.id_periodical = p.id WHERE id=(?)");
        String sql;

        SQLSale(String sql) {
            this.sql = sql;
        }
    }
}
