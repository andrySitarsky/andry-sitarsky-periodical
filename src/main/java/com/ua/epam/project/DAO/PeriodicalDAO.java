package com.ua.epam.project.DAO;

import com.ua.epam.project.beans.Catalog;
import com.ua.epam.project.beans.Periodical;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalDate;
import java.util.List;

public class PeriodicalDAO implements DAO<Periodical,Integer, ResultSet> {
    private Connection conn;
    private Logger logger = LogManager.getLogger(PeriodicalDAO.class);

    public PeriodicalDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public int create(Periodical periodical) {
        int id = -1;
        try (PreparedStatement stmt = conn.prepareStatement(SQLPeriodical.INSERT.sql)){
            stmt.setString(1, periodical.getName());
            stmt.setString(2, periodical.getPathImage());
            stmt.setString(3, periodical.getInfo());
            stmt.setString(4, periodical.getFindName());
            try (ResultSet rs = stmt.executeQuery()){
                if (rs.next()) id = rs.getInt("id");}
        } catch (SQLException e) {
            logger.error("create method " + e);
        }
        return id;
    }

    public void createCatalogsOfPeriodical(int id, List<Catalog> list){
        try (PreparedStatement stmt = conn.prepareStatement(buildSQL(list,
                SQLPeriodical.INSERT_CATALOGS.sql, id))){
            for (int i = 0; i < list.size(); i++) {
                stmt.setInt(i+1, list.get(i).getId());
            }
            stmt.executeQuery().next();
        } catch (SQLException e) {
            logger.error("create of periodical "+ e);
        }
    }

    @Override
    public Periodical read(Integer id) {
        Periodical periodical = null;
        try (PreparedStatement stmt = conn.prepareStatement(SQLPeriodical.GET.sql)){
            stmt.setInt(1, id);
            stmt.setDate(2, Date.valueOf(LocalDate.now()));
            stmt.setDate(3, Date.valueOf(LocalDate.now()));
            try(ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) periodical = getResult(rs);
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return periodical;

    }

    @Override
    public Periodical getResult(ResultSet rs) throws SQLException {
        return new Periodical().buildId(rs.getInt("id"))
                .buildName(rs.getString("name"))
                .buildPathImage(rs.getString("path_img"))
                .buildInfo(rs.getString("info"))
                .buildPrice(rs.getBigDecimal("price"));
    }

    @Override
    public boolean update(Periodical periodical) {
        boolean isSuccess= false;
        try (PreparedStatement stmt = conn.prepareStatement(SQLPeriodical.UPDATE.sql)){
            stmt.setString(1, periodical.getName());
            stmt.setString(2, periodical.getPathImage());
            stmt.setString(3, periodical.getInfo());
            stmt.setString(4, periodical.getFindName());
            stmt.setInt(5, periodical.getId());
            isSuccess = stmt.executeQuery().next();
        } catch (SQLException e) {
            logger.error(e);
        }
        return isSuccess;
    }

    @Override
    public boolean delete(Periodical periodical) {
        boolean isSuccess= false;
        try (PreparedStatement stmt = conn.prepareStatement(SQLPeriodical.DELETE.sql)){
            stmt.setInt(1, periodical.getId());
            isSuccess = stmt.executeQuery().next();
        } catch (SQLException e) {
            logger.error(e);
        }
        return isSuccess;
    }

    public boolean deleteCatalogsOfPeriodical(int id, List<Catalog> list){
        boolean isSuccess= false;
        PreparedStatement stmt = null;
        try {
            conn.setAutoCommit(false);
            stmt = conn.prepareStatement(SQLPeriodical.DELETE_CATALOGS.sql);
            for (Catalog c : list) {
                stmt.setInt(1, id);
                stmt.setInt(2, c.getId());
                stmt.addBatch();
            }
            int[] sam = stmt.executeBatch();
            conn.commit();
            isSuccess = true;
        } catch (BatchUpdateException ex){
            int[] updateCount = ex.getUpdateCounts();
            int count = 1;
            for (int i :
                    updateCount) {
                if (i == Statement.EXECUTE_FAILED) {
                    logger.error("Error on request " + count +": Execute failed");
                }
                count++;
            }
            try {
                conn.rollback();
            } catch (SQLException e) {
                logger.error(e);
            }
        } catch (SQLException e) {
            logger.error("delete catalog of periodical "+e);
        } finally {
            try {
                conn.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    logger.error(e);
                }
            }
        }
        return isSuccess;
    }

    private String buildSQL(List<Catalog> list, String request, int id) {
        StringBuilder sql = new StringBuilder().append(request);
        for (Catalog catalog : list) {sql.append("(").append(id).append(",?),");}
        int lastSeparator = sql.lastIndexOf(",");
        sql.delete(lastSeparator,lastSeparator+1);
        sql.append("RETURNING id_periodical");
        return sql.toString();
    }
    enum SQLPeriodical {
        INSERT("INSERT INTO periodicals (id,name,path_img,info,find_name) VALUES(DEFAULT,(?),(?),(?),(?))" +
                " RETURNING id"),
        INSERT_CATALOGS("INSERT INTO periodical_catalog (id_periodical,id_catalog) VALUES "),
        DELETE("DELETE FROM periodicals WHERE id=(?) RETURNING id"),
        DELETE_CATALOGS("DELETE FROM periodical_catalog WHERE id_periodical=(?) AND id_catalog=(?)"),
        UPDATE("UPDATE periodicals SET name=(?), path_img=(?), info=(?), find_name=(?) WHERE id=(?) RETURNING id"),
        GET("SELECT p.id AS id,p.name AS name,p.path_img AS path_img,p.info AS info,pl.price AS price " +
                "FROM periodicals p JOIN price_list pl ON p.id = pl.id_periodical " +
                "WHERE p.id=(?) AND pl.data_start < (?) AND pl.data_end > (?)");
        String sql;
        SQLPeriodical(String sql) {
            this.sql = sql;
        }
    }
}
