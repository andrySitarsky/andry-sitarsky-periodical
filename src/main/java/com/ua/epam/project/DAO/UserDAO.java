package com.ua.epam.project.DAO;

import com.ua.epam.project.beans.User;
import com.ua.epam.project.enums.Role;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAO implements DAO<User, Integer, ResultSet>{
    private Connection conn;
    private Logger log = LogManager.getLogger(UserDAO.class);

    public UserDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public int create(User user) {
        int id = -1;
        try (PreparedStatement stmt = conn.prepareStatement(SQLUser.INSERT.sql)){
            stmt.setString(1, user.getRole().name());
            stmt.setString(2, user.getNickName());
            stmt.setString(3, user.getLogin());
            stmt.setString(4, user.getPassword());
            stmt.setString(5, user.getEmail());
            stmt.setBigDecimal(6, user.getBalance());
            stmt.setBoolean(7, user.isBlocked());
            try (ResultSet rs = stmt.executeQuery()){
                if (rs.next()) id = rs.getInt("id");}
        } catch (SQLException e) {
            log.error(e);
        }
        return id;
    }

    @Override
    public User read(Integer id) {
        User user = null;
        try (PreparedStatement stmt = conn.prepareStatement(SQLUser.GET.sql)){
            stmt.setInt(1, id);
            try(ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) user = getResult(rs);
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return user;
    }

    @Override
    public User getResult(ResultSet rs) throws SQLException {
        return new User().buildId(rs.getInt("id"))
                .buildRole(Role.valueOf(rs.getString("role")))
                .buildNickName(rs.getString("nick_name"))
                .buildLogin(rs.getString("login"))
                .buildPassword(rs.getString("password"))
                .buildEmail(rs.getString("e_mail"))
                .buildBalance(rs.getBigDecimal("balance"))
                .buildBlocked(rs.getBoolean("is_blocked"));
    }

    @Override
    public boolean update(User user) {
        boolean isSuccess= false;
        try (PreparedStatement stmt = conn.prepareStatement(SQLUser.UPDATE.sql)){
            stmt.setString(1, user.getRole().name());
            stmt.setString(2, user.getNickName());
            stmt.setString(3, user.getLogin());
            stmt.setString(4, user.getPassword());
            stmt.setString(5, user.getEmail());
            stmt.setBigDecimal(6, user.getBalance());
            stmt.setBoolean(7, user.isBlocked());
            stmt.setInt(8, user.getId());
            isSuccess = stmt.executeQuery().next();
        } catch (SQLException e) {
            log.error(e);
        }
        return isSuccess;
    }

    @Override
    public boolean delete(User user) {
        boolean isSuccess= false;
        try (PreparedStatement stmt = conn.prepareStatement(SQLUser.DELETE.sql)){
            stmt.setInt(1, user.getId());
            isSuccess = stmt.executeQuery().next();
        } catch (SQLException e) {
            log.error(e);
        }
        return isSuccess;
    }

    public User getUser(String login, String password) {
        User user = null;
        try (PreparedStatement stmt = conn.prepareStatement(SQLUser.GET_USER.sql)){
            stmt.setString(1, login);
            stmt.setString(2, password);
            try(ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) user = getResult(rs);
            }
        } catch (SQLException e) {
            log.error(e);
        }
        return user;
    }

    public boolean addMoney(User user) {
        boolean isSuccess= false;
        try (PreparedStatement stmt = conn.prepareStatement(SQLUser.ADD_MONEY.sql)){
            stmt.setBigDecimal(1, user.getBalance());
            stmt.setInt(2, user.getId());
            isSuccess = stmt.executeQuery().next();
        } catch (SQLException e) {
            log.error(e);
        }
        return isSuccess;
    }

    public boolean updateBalance(int id, BigDecimal balance) {
        boolean isSuccess= false;
        try (PreparedStatement stmt = conn.prepareStatement(SQLUser.UPDATE_BALANCE.sql)){
            stmt.setBigDecimal(1, balance);
            stmt.setInt(2, id);
            isSuccess = stmt.executeQuery().next();
        } catch (SQLException e) {
            log.error(e);
        }
        return isSuccess;
    }

    public boolean changeRole(int user_id, String name) {
        boolean isSuccess= false;
        try (PreparedStatement stmt = conn.prepareStatement(SQLUser.UPDATE_ROLE.sql)){
            stmt.setString(1, name);
            stmt.setInt(2, user_id);
            isSuccess = stmt.executeQuery().next();
        } catch (SQLException e) {
            log.error(e);
        }
        return isSuccess;
    }

    public boolean blocked(int id, boolean blocked) {
        boolean isSuccess= false;
        try (PreparedStatement stmt = conn.prepareStatement(SQLUser.BLOCKED.sql)){
            stmt.setBoolean(1, blocked);
            stmt.setInt(2, id);
            isSuccess = stmt.executeQuery().next();
        } catch (SQLException e) {
            log.error(e);
        }
        return isSuccess;
    }

    enum SQLUser{
        INSERT("INSERT INTO users (id,role,nick_name,login,password,e_mail,balance,is_blocked) " +
                "VALUES(DEFAULT,?,?,?,?,?,?,?) RETURNING id"),
        DELETE("DELETE FROM users WHERE id=(?) RETURNING id"),
        UPDATE_BALANCE("UPDATE users SET balance=(?) WHERE id=(?) RETURNING id"),
        UPDATE_ROLE("UPDATE users SET role=(?) WHERE id=(?) RETURNING id"),
        UPDATE("UPDATE users SET role=(?), nick_name=(?), login=(?), password=(?), e_mail=(?), balance=(?)," +
                " is_blocked=(?) WHERE id=(?) RETURNING id"),
        GET("SELECT * FROM users WHERE id=(?)"),
        GET_USER("SELECT * FROM users WHERE login=(?) AND password=(?)"),
        ADD_MONEY("UPDATE users SET  balance=(?) WHERE id=(?) RETURNING id"),
        BLOCKED("UPDATE users SET  is_blocked=(?) WHERE id=(?) RETURNING id");
        String sql;

        SQLUser(String sql) {
            this.sql = sql;
        }
    }
}
