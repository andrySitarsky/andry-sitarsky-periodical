package com.ua.epam.project.serves;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.jsp.jstl.fmt.LocalizationContext;

public class PaginationServes {
    private Logger log = LogManager.getLogger(PaginationServes.class);
    public int getNumberPage(int pageHome, String pageStr, LocalizationContext locale, int countPeriodical, int numberPage){
        int page = pageHome;
        if (pageStr.equals(locale.getResourceBundle().getString("pagination.previous"))) {
            if (page > 1) {
                page--;
            }
        } else if (pageStr.equals(locale.getResourceBundle().getString("pagination.next"))) {
            if (page < Math.ceil(countPeriodical / (double) numberPage)) {
                page++;
            }
        } else {
            try {
                page = Integer.parseInt(pageStr);
            } catch (NumberFormatException e) {
                log.error(e);
            }
        }
        return page;
    }
}
