package com.ua.epam.project.serves;

import com.ua.epam.project.beans.User;
import com.ua.epam.project.beans.UserInfo;
import com.ua.epam.project.beans.decorator.DecorUser;
import com.ua.epam.project.db.Database;
import com.ua.epam.project.enums.Role;
import com.ua.epam.project.repo.UserRepo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class UserServes {
    private UserRepo userRepo = new UserRepo(Database.getConnection());
    private Logger log = LogManager.getLogger(UserServes.class);

    public void addUser(User user, UserInfo userInfo) {
        user.buildBalance(BigDecimal.ZERO).buildBlocked(false);
        int id = userRepo.getUserDAO().create(user);
        if (id > 0){
            userInfo.buildId(id).buildTextBlocked("");
            if (userRepo.getUserInfoDAO().create(userInfo) < 1){userRepo.getUserDAO().delete(user);}
        }
    }

    public String getHashPassword(String str) {
        String newPassword = null;
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            newPassword = new String(sha1.digest(str.getBytes()));
        } catch (NoSuchAlgorithmException e) {
            log.error(e);
        }
        return newPassword;
    }

    public void editUser(User user, UserInfo userInfo) {
        userRepo.getUserDAO().update(user);
        userRepo.getUserInfoDAO().update(userInfo);
    }

    public User getUser(String login, String password) {
        return userRepo.getUserDAO().getUser(login, getHashPassword(password));
    }

    public UserInfo getUserInfo(int id) {
        return userRepo.getUserInfoDAO().read(id);
    }

    public void addMoney(User user) {
        userRepo.getUserDAO().addMoney(user);
    }

    public void setBalance(int id, BigDecimal balance) {
        userRepo.getUserDAO().updateBalance(id, balance);
    }
    public UserRepo getUserRepo(){return userRepo;}

    public List<DecorUser> getUsers() {
        return userRepo.getAll();
    }

    public User getUser(int id) {
        return userRepo.getUserDAO().read(id);
    }

    public void changeRole(int user_id, Role role) {
        userRepo.getUserDAO().changeRole(user_id, role.name());
    }

    public void setBlocked(int id, String text_info, boolean blocked) {
        userRepo.getUserDAO().blocked(id, blocked);
        userRepo.getUserInfoDAO().blockedInfo(id, text_info);
    }


}
