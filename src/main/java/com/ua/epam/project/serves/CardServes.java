package com.ua.epam.project.serves;

import com.ua.epam.project.beans.Catalog;
import com.ua.epam.project.beans.Periodical;
import com.ua.epam.project.beans.Price;
import com.ua.epam.project.beans.Topic;
import com.ua.epam.project.beans.decorator.DecorCatalog;
import com.ua.epam.project.db.Database;
import com.ua.epam.project.repo.CatalogRepo;
import com.ua.epam.project.repo.PeriodicalRepo;
import com.ua.epam.project.repo.PriceRepo;

import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CardServes {
    private PeriodicalRepo periodicalRepo = new PeriodicalRepo(Database.getConnection());
    private CatalogRepo catalogRepo = new CatalogRepo(Database.getConnection());
    private PriceRepo priceRepo = new PriceRepo(Database.getConnection());

    public Periodical getPeriodical(int id) {
        return periodicalRepo.getPeriodicalDAO().read(id);
    }

    public List<Catalog> getCatalogs(int id) {
        return catalogRepo.getCatalogsOfPeriodical(id);
    }

    public List<Price> getPrices(int id) {
        return priceRepo.getPriceOfPeriodical(id);
    }

    public ByteArrayOutputStream getCloneCatalog(List<Catalog> catalogs) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(catalogs);
        oos.close();
        return baos;
    }

    public ByteArrayOutputStream getClonePrice(List<Price> prices) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(prices);
        oos.close();
        return baos;
    }

    public void deleteCatalogs(List<Catalog> catalogs, List<Catalog> catalogsDelete) {
        catalogs.removeIf(e -> catalogsDelete.contains(e));
    }

    public List<Catalog> getCatalogs(ByteArrayOutputStream oldCatalogsCard) {
        ByteArrayInputStream bais = new ByteArrayInputStream(oldCatalogsCard.toByteArray());
        ObjectInputStream ois = null;
        try {
            ois = new ObjectInputStream(bais);
            return (List<Catalog>) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return new ArrayList<Catalog>();
    }

    /**
     * Check old list of catalogs and new list of catalog, then delete unnecessary value and to add new value
     *
     * @param oldCatalog Catalog of before changing
     * @param catalogs   Catalog of after changing
     * @param id         Periodical id
     */
    public void checkCatalogs(List<Catalog> oldCatalog, List<Catalog> catalogs, int id) {
        List<Catalog> deleteList = oldCatalog.stream().filter(e -> !catalogs.contains(e)).collect(Collectors.toList());
        if (!deleteList.isEmpty()) periodicalRepo.getPeriodicalDAO().deleteCatalogsOfPeriodical(id, deleteList);
        List<Catalog> addList = catalogs.stream().filter(e -> !oldCatalog.contains(e)).collect(Collectors.toList());
        if (!addList.isEmpty()) periodicalRepo.getPeriodicalDAO().createCatalogsOfPeriodical(id, addList);
    }

    public void updatePrices(List<Price> updateListPrice) {
        priceRepo.updatePrices(updateListPrice);
    }

    public void deletePrices(List<Price> deleteList) {
        priceRepo.delete(deleteList);
    }

    public void addPrices(List<Price> addPrice, int id) {
        priceRepo.createPrices(addPrice, id);
    }

    public void updatePice(Periodical newPeriodical) {
        periodicalRepo.getPeriodicalDAO().update(newPeriodical);
    }

    public int addPrice(Periodical newPeriodical) {
        return periodicalRepo.getPeriodicalDAO().create(newPeriodical);
    }

    public List<Periodical> findCardsForName(String nameToggle, String priceToggle, String nameFind, int... setting) {
        return periodicalRepo.getPeriodicalsOfName(nameToggle, priceToggle, nameFind, setting);
    }

    public List<Periodical> getAll(String nameToggle, String priceToggle, int... setting) {
        return periodicalRepo.getAll(nameToggle, priceToggle, setting);
    }

    public List<Periodical> getPeriodicals(List<DecorCatalog> list, String nameToggle, String priceToggle, int... setting) {
        return periodicalRepo.getPeriodicalsOfCatalog(list, nameToggle, priceToggle, setting);
    }

    public int getCountPeriodicalOfCatalog(List<DecorCatalog> codeTopic) {
        return periodicalRepo.getCountPeriodicalsOfCatalog(codeTopic);
    }

    public int getCountAll() {
        return periodicalRepo.getCountAll();
    }

    public int getCountPeriodicalOfName(String nameFind) {
        return periodicalRepo.getCountPeriodicalOfName(nameFind);
    }

    /*Depricate*/
    public List<Topic> getPagination(int countPeriodical, int countCard, int page, LocalizationContext locale) {
        List<Topic> list = new ArrayList<>();
        int numberPages = (int) Math.ceil(countPeriodical / (double) countCard);
        list.add(new Topic(false, locale.getResourceBundle().getString("pagination.previous")));
        for (int i = 0; i < numberPages; i++) {
            list.add(new Topic(i == (page - 1), (i + 1) + ""));
        }
        list.add(new Topic(false, locale.getResourceBundle().getString("pagination.next")));
        return list;
    }

    /*Depricate*/
    public String transformationString(String nameFind) {
        return nameFind.trim().replaceAll("\\s*", "").toLowerCase();
    }

    public void deletePeriodicals(List<Periodical> periodicals) {
        periodicalRepo.delete(periodicals);
    }

    public void writePrice(List<Price> newPrices, List<Price> oldPrices, int idPeriodical) {
        List<Price> updateList = new ArrayList<>();
        List<Price> addList = new ArrayList<>();
        for (int i = 0; i < newPrices.size(); i++) {
            if (oldPrices.size() > i && !oldPrices.contains(newPrices.get(i))
                    && oldPrices.get(i).getId() > 0) {
                updateList.add(newPrices.get(i));
            } else if ((oldPrices.size() > i && oldPrices.get(i).getId() == 0)
                    || oldPrices.size() <= i) {
                addList.add(newPrices.get(i));
            }
        }
        List<Price> deleteList = new ArrayList<>();
        for (int i = newPrices.size(); i < oldPrices.size(); i++) {
            deleteList.add(oldPrices.get(i));
        }
        if (!updateList.isEmpty()) {
            updatePrices(updateList);
        }
        if (!addList.isEmpty()) {
            addPrices(addList, idPeriodical);
        }
        if (!deleteList.isEmpty()) {
            deletePrices(deleteList);
        }

    }
}
