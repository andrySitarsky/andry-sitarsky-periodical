package com.ua.epam.project.serves;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public interface ServesProcess {
    void process(HttpServletRequest req) throws IOException;
}
