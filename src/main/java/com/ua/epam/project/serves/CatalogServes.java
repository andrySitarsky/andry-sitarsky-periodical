package com.ua.epam.project.serves;

import com.ua.epam.project.beans.Catalog;
import com.ua.epam.project.beans.Topic;
import com.ua.epam.project.beans.decorator.DecorCatalog;
import com.ua.epam.project.db.Database;
import com.ua.epam.project.repo.CatalogRepo;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class CatalogServes {
    private static final String SEPARATOR = "-";
    private List<DecorCatalog> catalogs = new ArrayList<>();
    private CatalogRepo catalogRepo = new CatalogRepo(Database.getConnection());

    public List<DecorCatalog> createListCatalog(){
        List<Catalog> catalogsRepo = catalogRepo.getAll();
        int level = 1;
        AtomicInteger countFirst = new AtomicInteger(1);
        catalogsRepo.stream().filter(e -> e.getLevel() == 1).forEach(
                el -> {
                    createFolder(el, catalogsRepo, countFirst.get(), new StringBuilder(),level);
                    countFirst.getAndIncrement();
                });

        catalogs.sort(Comparator.comparing(DecorCatalog::getLevel));
        return catalogs;
    }

    private void createFolder(Catalog catalog, List<Catalog> catalogsRepo, int countFirst,
                              StringBuilder builder, int level) {
        int id = catalog.getId();
        AtomicInteger count = new AtomicInteger(1);
        catalogs.add(new DecorCatalog().buildCatalog(catalog).buildLevel(builder.append(countFirst).toString()));
        List<Catalog> nextCatalog = catalogsRepo.stream().filter(e -> e.getParent() == id &&
                e.getLevel() == level+1).collect(Collectors.toList());
        if (!nextCatalog.isEmpty()){
            builder.append("-");
            nextCatalog.forEach(e -> {createFolder(e, catalogsRepo, count.get(), builder, level+1);
                builder.deleteCharAt(builder.length()-1);
                count.getAndIncrement();
            });
        }
    }

    public List<DecorCatalog> getCatalogs() {
        return catalogs;
    }



    public List<List<Topic>> getParentsCatalogs(String path) {
        String newPath = path.trim().substring(0,path.length()-1);
        return getCatalogs(newPath);
    }

    public Catalog getCatalog(String path) {
        return catalogs.stream().filter(element -> element.getLevel().equals(path))
                .findAny().orElse(null).getCatalog();
    }

    public DecorCatalog getCatalog(int oldIdCatalog) {
        return catalogs.stream().filter(element -> element.getId() == oldIdCatalog)
                .findAny().orElse(null);
    }
    public List<Catalog> getSimpleCatalogs(List<DecorCatalog> list){
        return list.stream().map(e -> e.getCatalog()).collect(Collectors.toList());
    }

    public List<List<Topic>> getCatalogs(String path) {
        List<List<Topic>> topicCatalogs = new ArrayList<>();
        if (path.isEmpty() || path.equals("0")) return topicCatalogs;

        /*Clear the path from non-numeric values*/
        List<String> newPath = Arrays.stream(path.trim().split(SEPARATOR))
                .filter(e -> e.matches("\\d*")).collect(Collectors.toList());

        /*Build catalogs*/
        StringBuilder buildPath = new StringBuilder();
        for (int i = 0; i < newPath.size(); i++) {
            if (i > 0) buildPath.append("-");
            int finalI = i;
            List<Topic> topics = catalogs.stream().filter(element -> element.getLevel().length() == 1 + finalI*2 &&
                            element.getLevel().startsWith(buildPath.toString()))
                    .map(element1-> new Topic(element1.getLevel().contentEquals(buildPath+(newPath.get(finalI))),
                            element1.getUaName())).collect(Collectors.toList());
            topicCatalogs.add(topics);
            buildPath.append(newPath.get(finalI));
        }
        topicCatalogs.removeIf(List::isEmpty);//delete empty catalog
        return topicCatalogs;
    }

    public void update(Catalog catalog) {
        catalogRepo.getCatalogDAO().update(catalog);
    }

    public int add(DecorCatalog catalog) {
        catalogs.add(catalog);
        return catalogRepo.getCatalogDAO().create(catalog.getCatalog());
    }
    public String getNewPath(){
        return catalogs.stream().filter(e -> e.getLevel().length() == 1)
                .max(Comparator.comparing(DecorCatalog::getLevel)).map(e -> Integer.parseInt(e.getLevel()) + 1)
                .orElse(1).toString();
    }

    public String getNewPath(DecorCatalog parent) {
        int newNumber = (int) catalogs.stream().filter(e -> e.getLevel().startsWith(parent.getLevel()) &&
                e.getCatalog().getLevel() == parent.getCatalog().getLevel()+1).count();
        return new StringBuilder().append(parent.getLevel()).append("-").append(newNumber+1).toString();
    }

    public List<DecorCatalog> getCatalogs(HttpServletRequest req) {
        return catalogs.stream().filter(e -> req.getParameter("option"+e.getId()) != null)
                .collect(Collectors.toList());
    }

    public void delete(HttpServletRequest req) {
        List<DecorCatalog> listDelete = getCatalogs(req);
        catalogs.removeIf(listDelete::contains);
        catalogRepo.delete(listDelete.stream().map(DecorCatalog::getCatalog).collect(Collectors.toList()));

    }


    public List<DecorCatalog> getChildrenCatalog(String codTopic) {
        return catalogs.stream().filter(e -> e.getLevel().startsWith(codTopic)).collect(Collectors.toList());
    }

    public List<DecorCatalog> getCatalogs(List<Catalog> list) {
        return catalogs.stream().filter(e -> list.contains(e.getCatalog())).collect(Collectors.toList());
    }
}
