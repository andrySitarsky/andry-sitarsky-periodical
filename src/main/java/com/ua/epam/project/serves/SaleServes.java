package com.ua.epam.project.serves;

import com.ua.epam.project.beans.Sale;
import com.ua.epam.project.beans.User;
import com.ua.epam.project.db.Database;
import com.ua.epam.project.repo.SaleRepo;
import com.ua.epam.project.repo.UserRepo;

import java.math.BigDecimal;
import java.time.Period;
import java.util.List;

public class SaleServes {
    private SaleRepo saleRepo = new SaleRepo(Database.getConnection());

    public List<Sale> getSalesUser(int id) {
        return saleRepo.getSalesOfUser(id);
    }

    public Sale getSale(int id) {
        return null;
    }

    public void setSale(Sale sale) {
        saleRepo.getSaleDAO().create(sale);
    }

    public boolean salePeriodical(Sale sale, User user, UserRepo userRepo) {
        saleRepo.salePeriodical(sale.buildIdUser(user.getId()),
                userRepo,user.getId(), user.getBalance().subtract(sale.getAmount()));
        return true;
    }

    public BigDecimal getAmount(Sale sale, String price) {
        double priceSale = Double.parseDouble(price);
        int month = Period.between(sale.getDateStart(),sale.getDateEnd()).getMonths();
        double sum = priceSale * sale.getNumber() * month;
        return BigDecimal.valueOf(sum);
    }
}
