package com.ua.epam.project.repo;

import java.sql.SQLException;
import java.util.List;

public interface Repository <Entity, Result>{
    List<Entity> getResult(Result result) throws SQLException;
    boolean delete(List<Entity> list);

    default String buildSQL(List<Entity> list, String request) {
        StringBuilder sql = new StringBuilder().append(request);
        for (Entity entity : list) {sql.append("?,");}
        int lastSeparator = sql.lastIndexOf(",");
        sql.replace(lastSeparator,lastSeparator+1,")");
        sql.append(" RETURNING id");
        return sql.toString();
    }
}
