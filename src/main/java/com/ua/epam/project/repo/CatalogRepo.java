package com.ua.epam.project.repo;

import com.ua.epam.project.DAO.CatalogDAO;
import com.ua.epam.project.beans.Catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CatalogRepo implements Repository<Catalog, ResultSet>{
    private Connection conn;
    private CatalogDAO catalogDAO;

    public CatalogRepo(Connection conn) {
        this.conn = conn;
        catalogDAO = new CatalogDAO(conn);
    }

    public CatalogDAO getCatalogDAO() {
        return catalogDAO;
    }


    public List<Catalog> getAll() {
        List<Catalog> list = null;
        try (PreparedStatement stmt = conn.prepareStatement(SQLCatalog.GET_ALL.sql);
             ResultSet rs = stmt.executeQuery()){
             list = getResult(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Catalog> getCatalogsOfPeriodical(int id){
        List<Catalog> list = null;
        try (PreparedStatement stmt = conn.prepareStatement(SQLCatalog.GET_PERIODICAL.sql)){
            stmt.setInt(1, id);
             try (ResultSet rs = stmt.executeQuery()){
                 list = getResult(rs);
             }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public List<Catalog> getResult(ResultSet rs) throws SQLException {
        List<Catalog> list = new ArrayList<>();
        while (rs.next()){
            list.add(catalogDAO.getResult(rs));
        }
        return list;
    }

    @Override
    public boolean delete(List<Catalog> list) {
        boolean isSuccess= false;
        try (PreparedStatement stmt = conn.prepareStatement(buildSQL(list, SQLCatalog.DELETE.sql))){
            for (int i = 0; i < list.size(); i++) {
                stmt.setInt(i+1, list.get(i).getId());
            }
           isSuccess = stmt.executeQuery().next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isSuccess;
    }

    enum SQLCatalog{
        DELETE("DELETE FROM catalogs WHERE id IN("),
        GET_PERIODICAL("SELECT c.id AS id, c.level AS level, c.name_ua AS name_ua, c.name_en AS name_en, " +
                "c.parent AS parent FROM catalogs c JOIN periodical_catalog pc ON pc.id_catalog = c.id " +
                "WHERE pc.id_periodical=(?)"),
        GET_ALL("SELECT * FROM catalogs");
        String sql;
        SQLCatalog(String sql) {
            this.sql = sql;
        }
    }
}
