package com.ua.epam.project.repo;

import com.ua.epam.project.DAO.UserDAO;

import com.ua.epam.project.DAO.UserInfoDAO;
import com.ua.epam.project.beans.decorator.DecorUser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRepo implements Repository<DecorUser, ResultSet>{
    private Connection conn;
    private UserDAO userDAO;
    private UserInfoDAO userInfoDAO;
    private Logger log = LogManager.getLogger(UserRepo.class);

    public UserRepo(Connection conn) {
        this.conn = conn;
        userDAO = new UserDAO(conn);
        userInfoDAO = new UserInfoDAO(conn);
    }

    public UserDAO getUserDAO() {
        return userDAO;
    }

    public UserInfoDAO getUserInfoDAO() {
        return userInfoDAO;
    }

    public List<DecorUser> getAll(){
        List<DecorUser> list = null;
        try (PreparedStatement stmt = conn.prepareStatement(SQLUsers.GET_All.sql);
        ResultSet rs = stmt.executeQuery()){
            list = getResult(rs);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public List<DecorUser> getResult(ResultSet rs) throws SQLException {
        List<DecorUser> list = new ArrayList<>();
        while (rs.next()) {
            list.add(new DecorUser().buildUser(userDAO.getResult(rs))
                    .buildCount(rs.getInt("count"))
                    .buildSum(rs.getBigDecimal("sum")));
        }
        return list;
    }

    @Override
    public boolean delete(List<DecorUser> list) {
        return false;
    }
    enum SQLUsers{
        GET_All("SELECT *, (SELECT COUNT(id_periodical) FROM sale s WHERE u.id=id_user GROUP BY id_user) AS count," +
                " (SELECT SUM(amount) FROM sale s WHERE u.id=id_user GROUP BY id_user) AS sum FROM users u");

        String sql;
        SQLUsers(String sql) {
            this.sql = sql;
        }
    }
}
