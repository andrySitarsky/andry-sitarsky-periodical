package com.ua.epam.project.repo;

import com.ua.epam.project.DAO.SaleDAO;
import com.ua.epam.project.beans.Catalog;
import com.ua.epam.project.beans.Sale;
import com.ua.epam.project.beans.User;
import com.ua.epam.project.db.Database;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SaleRepo implements Repository<Sale, ResultSet>{
    private Connection conn;
    private SaleDAO saleDAO;
    private Logger log = LogManager.getLogger(SaleRepo.class);

    public SaleRepo(Connection conn) {
        this.conn = conn;
        saleDAO = new SaleDAO(conn);
    }

    public SaleDAO getSaleDAO() {
        return saleDAO;
    }

    public List<Sale> getSalesOfUser(int id){
        List<Sale> list = null;
        try (PreparedStatement stmt = conn.prepareStatement(SQLSale.GET.sql)){
            stmt.setInt(1, id);
            try (ResultSet rs = stmt.executeQuery()){
                list = getResult(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
    public boolean salePeriodical(Sale sale, UserRepo userRepo, int id, BigDecimal balance){
        Connection conn = Database.getConnection();
        try {
            conn.setAutoCommit(false);
            getSaleDAO().create(sale);
            userRepo.getUserDAO().updateBalance(id, balance);
            conn.commit();
        } catch (SQLException e) {
            try {
                conn.rollback();
            } catch (SQLException ex) {
                log.error(ex);
            }
            log.error(e);
            return false;
        } finally {
            try {
                conn.setAutoCommit(true);
                if (conn != null){conn.close();}
            } catch (SQLException e) {
                log.error(e);
            }
        }
        return true;
    }

    @Override
    public List<Sale> getResult(ResultSet rs) throws SQLException {
        List<Sale> sales = new ArrayList<>();
        while (rs.next()){
            sales.add(saleDAO.getResult(rs));
        }
        return sales;
    }

    @Override
    public boolean delete(List<Sale> list) {
        boolean isSuccess = false;
        try (PreparedStatement stmt = conn.prepareStatement(buildSQL(list,SQLSale.DELETE.sql))) {
            for (int i = 0; i < list.size(); i++) {
                stmt.setInt(i + 1, list.get(i).getId());
            }
            isSuccess = stmt.executeQuery().next();
        } catch (SQLException e) {
            log.error("delete " + e);
        }
        return isSuccess;
    }
    enum SQLSale{
        DELETE("DELETE FROM sale WHERE id IN("),
        GET("SELECT s.*, p.name FROM sale s JOIN periodicals p ON s.id_periodical = p.id WHERE s.id_user = (?)");
        String sql;

        SQLSale(String sql) {
            this.sql = sql;
        }
    }
}
