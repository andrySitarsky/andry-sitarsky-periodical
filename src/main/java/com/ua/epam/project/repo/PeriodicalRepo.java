package com.ua.epam.project.repo;

import com.ua.epam.project.DAO.PeriodicalDAO;
import com.ua.epam.project.beans.Periodical;
import com.ua.epam.project.beans.decorator.DecorCatalog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class PeriodicalRepo implements Repository<Periodical, ResultSet> {
    private Connection conn;
    private PeriodicalDAO periodicalDAO;
    private Logger logger = LogManager.getLogger(PriceRepo.class);

    public PeriodicalRepo(Connection conn) {
        this.conn = conn;
        periodicalDAO = new PeriodicalDAO(conn);
    }

    public PeriodicalDAO getPeriodicalDAO() {
        return periodicalDAO;
    }

    public List<Periodical> getAll(String nameToggle, String priceToggle, int... setting) {
        List<Periodical> list = null;
        try (PreparedStatement stmt = conn.prepareStatement(getSQL(nameToggle, priceToggle,
                SQLPeriodical.GET_ALL.sql))) {

            stmt.setDate(1, Date.valueOf(LocalDate.now()));
            stmt.setDate(2, Date.valueOf(LocalDate.now()));

            stmt.setInt(3, setting[0]);
            stmt.setInt(4, setting[1]);
            try (ResultSet rs = stmt.executeQuery()) {
                list = getResult(rs);
            }
        } catch (SQLException e) {
            logger.error("getAll " + e);
        }
        return list;
    }

    public int getCountAll() {
        int count = 0;
        try (PreparedStatement stmt = conn.prepareStatement(SQLPeriodical.COUNT_ALL.sql)) {

            stmt.setDate(1, Date.valueOf(LocalDate.now()));
            stmt.setDate(2, Date.valueOf(LocalDate.now()));


            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    count = rs.getInt("count");
                }
            }
        } catch (SQLException e) {
            logger.error("getCountAll " + e);
        }
        return count;
    }


    public List<Periodical> getPeriodicalsOfCatalog(List<DecorCatalog> catalogList, String nameToggle, String priceToggle, int... setting) {
        List<Periodical> list = null;
        try (PreparedStatement stmt = conn.prepareStatement(getSQL(nameToggle, priceToggle,
                buildCatalog(catalogList,SQLPeriodical.GET_CATALOG.sql)))) {
            int count = 1;
            for (DecorCatalog dc :
                    catalogList) {
                stmt.setInt(count++, dc.getId());
            }
            stmt.setDate(count++, Date.valueOf(LocalDate.now()));
            stmt.setDate(count++, Date.valueOf(LocalDate.now()));
            stmt.setInt(count++, setting[0]);
            stmt.setInt(count++, setting[1]);
            try (ResultSet rs = stmt.executeQuery()) {
                list = getResult(rs);
            }
        } catch (SQLException e) {
            logger.error("getPeriodical of catalog " + e);
        }
        return list;
    }

    private String buildCatalog(List<DecorCatalog> catalogList, String request) {
        StringBuilder sql = new StringBuilder().append(request);
        for (DecorCatalog entity : catalogList) {sql.append("?,");}
        int lastSeparator = sql.lastIndexOf(",");
        sql.replace(lastSeparator,lastSeparator+1,")");
        sql.append("AND pl.data_start < (?) AND pl.data_end > (?)");
        return sql.toString();
    }

    private String getSQL(String nameToggle, String priceToggle, String sql) {
        StringBuilder sqlBuilder = new StringBuilder().append(sql);
        if (nameToggle != null) {
            sqlBuilder.append(" ORDER BY p.name");
            if (nameToggle.equals("decrease")){sqlBuilder.append(" DESC");}
        } else if (priceToggle != null) {
            sqlBuilder.append(" ORDER BY pl.price");
            if (priceToggle.equals("decrease")){sqlBuilder.append(" DESC");}
        }
       sqlBuilder.append(" LIMIT ? OFFSET ?");
        return sqlBuilder.toString();
    }

    public int getCountPeriodicalsOfCatalog(List<DecorCatalog> list) {
        int result = 0;
        try (PreparedStatement stmt = conn.prepareStatement(
                buildCatalog(list, SQLPeriodical.COUNT_CATALOG.sql))) {
            int count = 1;
            for (DecorCatalog dc : list) {
                stmt.setInt(count++, dc.getId());
            }
            stmt.setDate(count++, Date.valueOf(LocalDate.now()));
            stmt.setDate(count++, Date.valueOf(LocalDate.now()));
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    result = rs.getInt("count");
                }
            }
        } catch (SQLException e) {
            logger.error("getCountPeriodicalsOfCatalog " + e);
        }
        return result;
    }

    public List<Periodical> getPeriodicalsOfName(String nameToggle, String priceToggle,String name, int... setting) {
        List<Periodical> list = null;
        try (PreparedStatement stmt = conn.prepareStatement(getSQL(nameToggle, priceToggle,
                SQLPeriodical.GET_NAME.sql))) {
            stmt.setString(1, "%" + name + "%");
            stmt.setDate(2, Date.valueOf(LocalDate.now()));
            stmt.setDate(3, Date.valueOf(LocalDate.now()));
            stmt.setInt(4, setting[0]);
            stmt.setInt(5, setting[1]);
            try (ResultSet rs = stmt.executeQuery()) {
                list = getResult(rs);
            }
        } catch (SQLException e) {
            logger.error("getPeriodicalsOfName " + e);
        }
        return list;
    }

    public int getCountPeriodicalOfName(String name) {
        int count = 0;
        try (PreparedStatement stmt = conn.prepareStatement(SQLPeriodical.COUNT_NAME.sql)) {
            stmt.setString(1, "%" + name + "%");
            stmt.setDate(2, Date.valueOf(LocalDate.now()));
            stmt.setDate(3, Date.valueOf(LocalDate.now()));

            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    count = rs.getInt("count");
                }
            }
        } catch (SQLException e) {
            logger.error("getCountPeriodicalofName " + e);
        }
        return count;
    }

    @Override
    public List<Periodical> getResult(ResultSet rs) throws SQLException {
        List<Periodical> list = new ArrayList<>();
        while (rs.next()) {
            list.add(periodicalDAO.getResult(rs));
        }
        return list;
    }

    @Override
    public boolean delete(List<Periodical> list) {
        boolean isSuccess = false;
        try (PreparedStatement stmt = conn.prepareStatement(buildSQL(list, SQLPeriodical.DELETE.sql))) {
            for (int i = 0; i < list.size(); i++) {
                stmt.setInt(i + 1, list.get(i).getId());
            }
            isSuccess = stmt.executeQuery().next();
        } catch (SQLException e) {
            logger.error("delete " + e);
        }
        return isSuccess;
    }

    enum SQLPeriodical {
        DELETE("DELETE FROM periodicals WHERE id IN("),
        COUNT_CATALOG("SELECT COUNT(*) count" +
                " FROM periodicals p JOIN periodical_catalog pc ON pc.id_periodical = p.id " +
                "JOIN price_list pl ON p.id = pl.id_periodical JOIN catalogs c ON pc.id_catalog = c.id " +
                "WHERE c.id IN ( "),
        GET_CATALOG("SELECT p.id AS id,p.name AS name,p.path_img AS path_img,p.info AS info,pl.price AS price " +
                " FROM periodicals p JOIN periodical_catalog pc ON pc.id_periodical = p.id " +
                "JOIN price_list pl ON p.id = pl.id_periodical JOIN catalogs c ON pc.id_catalog = c.id " +
                "WHERE c.id IN ( "),
        COUNT_NAME("SELECT COUNT(*)" +
                " FROM periodicals p JOIN price_list pl ON p.id = pl.id_periodical " +
                "WHERE p.find_name LIKE (?) AND pl.data_start < (?) AND pl.data_end > (?)"),
        GET_NAME("SELECT p.id id,p.name AS name,p.path_img AS path_img,p.info AS info,pl.price AS price" +
                " FROM periodicals p JOIN price_list pl ON p.id = pl.id_periodical " +
                "WHERE p.find_name LIKE (?) AND pl.data_start < (?) AND pl.data_end > (?) "),
        COUNT_ALL("SELECT COUNT(*)" +
                " FROM periodicals p JOIN price_list pl ON p.id = pl.id_periodical " +
                "WHERE pl.data_start < (?) AND pl.data_end > (?)"),
        GET_ALL("SELECT p.id AS id,p.name AS name,p.path_img AS path_img,p.info AS info,pl.price AS price" +
                " FROM periodicals p JOIN price_list pl ON p.id = pl.id_periodical " +
                "WHERE pl.data_start < (?) AND pl.data_end > (?) ");
        String sql;

        SQLPeriodical(String sql) {
            this.sql = sql;
        }
    }
}
