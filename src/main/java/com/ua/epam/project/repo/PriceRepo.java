package com.ua.epam.project.repo;

import com.ua.epam.project.beans.Price;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PriceRepo implements Repository<Price, ResultSet>{
    private Connection conn;
    private Logger logger = LogManager.getLogger();

    public PriceRepo(Connection conn) {
        this.conn = conn;
    }
    /**
     * @param id Id of periodical*/
    public boolean createPrices(List<Price> prices, int id){
        try (PreparedStatement stmt = conn.prepareStatement(SQLPrice.INSERT.sql)){
            for (Price price : prices) {
                stmt.setInt(1, id);
                stmt.setDate(2, Date.valueOf(price.getStartData()));
                stmt.setDate(3, Date.valueOf(price.getEndData()));
                stmt.setBigDecimal(4, price.getPrice());
                stmt.addBatch();
            }
            stmt.executeBatch();
        } catch (BatchUpdateException ex){
            int[] updateCount = ex.getUpdateCounts();
            int count = 1;
            for (int i :
                    updateCount) {
                if (i == Statement.EXECUTE_FAILED) {
                    logger.error("Error on request " + count +": Execute failed");
                }
                count++;
            }
            try {
                conn.rollback();
            } catch (SQLException e) {
                logger.error(e);
            }
            return false;
        } catch (SQLException e) {
            logger.error(e);
            return false;
        }

        return true;
    }
    /***/
    public boolean updatePrices(List<Price> prices){
        try (PreparedStatement stmt = conn.prepareStatement(SQLPrice.UPDATE.sql)){
            for (Price price : prices) {
                stmt.setDate(1, Date.valueOf(price.getStartData()));
                stmt.setDate(2, Date.valueOf(price.getEndData()));
                stmt.setBigDecimal(3, price.getPrice());
                stmt.setInt(4, price.getId());
                stmt.addBatch();
            }
            stmt.executeBatch();
        } catch (BatchUpdateException ex){
            int[] updateCount = ex.getUpdateCounts();
            int count = 1;
            for (int i :
                    updateCount) {
                if (i == Statement.EXECUTE_FAILED) {
                    logger.error("Error on request " + count +": Execute failed");
                }
                count++;
            }
            return false;
        } catch (SQLException e) {
            logger.error(e);
            return false;
        }

        return true;
    }

    public List<Price> getPriceOfPeriodical(int id){
        List<Price> list = null;
        try (PreparedStatement stmt = conn.prepareStatement(SQLPrice.GET_PERIODICAL.sql)){
            stmt.setInt(1, id);
            try (ResultSet rs = stmt.executeQuery()){
                list = getResult(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public List<Price> getResult(ResultSet rs) throws SQLException {
        List<Price> list = new ArrayList<>();
        while (rs.next()){
            Price price = new Price().buildId(rs.getInt("id"))
                    .buildStartData(rs.getDate("data_start").toLocalDate())
                    .buildEndData(rs.getDate("data_end").toLocalDate())
                    .buildPrice(rs.getBigDecimal("price"));
            list.add(price);
        }
        return list;
    }

    @Override
    public boolean delete(List<Price> list) {
        boolean isSuccess= false;
        try (PreparedStatement stmt = conn.prepareStatement(buildSQL(list, SQLPrice.DELETE.sql))){
            for (int i = 0; i < list.size(); i++) {
                stmt.setInt(i+1, list.get(i).getId());
            }
            isSuccess = stmt.executeQuery().next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isSuccess;
    }


    enum SQLPrice {
        DELETE("DELETE FROM price_list WHERE id IN("),
        INSERT("INSERT INTO price_list (id,id_periodical,data_start,data_end,price) VALUES (DEFAULT,?,?,?,?)"),
        GET_PERIODICAL("SELECT * FROM price_list WHERE id_periodical=?"),
        GET_ALL("SELECT * FROM price_list"),
        UPDATE("UPDATE price_list SET data_start=(?), data_end=(?), price=(?) WHERE id=(?)");
        String sql;
        SQLPrice(String sql) {
            this.sql = sql;
        }
    }
}
