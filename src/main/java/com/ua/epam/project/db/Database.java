package com.ua.epam.project.db;

import org.postgresql.ds.PGConnectionPoolDataSource;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class Database {
	private static Database database;
	public static Database instance(){
		if (database == null) database = new Database();
		return database;
	}
	public static Connection getConnection() {
		Connection conn= null;
		 try {
			 Properties properties = new Properties();
			 InputStream stream = Database.class.getResourceAsStream("/app.properties");
			 properties.load(stream);
			 PGConnectionPoolDataSource pg = new PGConnectionPoolDataSource();
			 pg.setURL(properties.getProperty("connection.url"));
			 conn = pg.getConnection();
		} catch (SQLException | IOException ex) {
			 ex.printStackTrace();
		 }
		return conn;
	}

}
