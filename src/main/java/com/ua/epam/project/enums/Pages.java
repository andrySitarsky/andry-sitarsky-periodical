package com.ua.epam.project.enums;

public enum Pages {HOME("home/index","periodical?command=home.Home"),
    CARD("card/card_page","periodical?command=guest.Card"),
    CARD_CATALOG("card/card_catalog"),
    SALE_MODAL("home/sale_modal"),
    SING("sing/sing_in", "periodical?command=guest.Sing"),
    REGISTRATION("registration/registration","periodical?command=guest.Registration"),
    USER("user/user", "periodical?command=User"),
    SUBSCRIBE("subscribe/subscribe", "periodical?command=Subscribe"),
    MODAL("home/modal_catalog", "periodical?command=admin.Modal"),
    ADMIN("admin/admin", "periodical?command=admin.AdminPage"),
    AMOUNT("home/amount"),
    BLOCKED("sing/blocked"),
    ADMIN_ERROR("admin/error");

    private String path;
    private String command;

    Pages(String path) {
        this.path = path;
    }

    Pages(String path, String command) {
        this.path = path;
        this.command = command;
    }

    public String getPath(){return path;}

    public String getCommand(){return command;}
}
